<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.07.2016
 * Time: 15:57
 */

namespace console\components;

use SimpleXMLReader;

class SnilsXmlReader extends SimpleXMLReader
{

    public $zl = [];
    public $zl_count = 0;
    public $zl_inserted = 0;
    public $n_start = 0;

    public function __construct()
    {

        // by xpath
        $this->registerCallback("/snils_zl_list/snils", array($this, "callbackZL"));
        $this->registerCallback("/snils_zl_list/zglv", array($this, "callbackZGLV"));

    }

    public function insertZl($info = true)
    {
        $i = count($this->zl);
        if ($i == 0) {
            return;
        }
        \Yii::$app->db->createCommand()->batchInsert('temp.dbo.ffoms_worked_snils', [

            'snils',

        ], $this->zl)->execute();
        $this->zl = [];
        $this->zl_inserted += $i;
        if ($info)
            echo $this->zl_inserted . ' from ' . $this->zl_count . PHP_EOL;
    }

    protected function callbackZGLV($reader)
    {
        $xml = $reader->expandSimpleXml();
        $this->zl_count = (int)$this->getSafeValue($xml, 'nrec');
        return true;
    }

    /**
     * @param $reader self
     * @return bool
     */
    protected function callbackZL($reader)
    {
        $xml = $reader->expandSimpleXml();
        $a = [];
        $a['snils'] = (string)$xml;// $this->getSafeValue($xml, 'snils');
        $this->zl[] = $a;

        if (count($this->zl) >= 1000) {
            $this->insertZl();
        }
        return true;
    }



    protected function getSafeValue($xml, $attr)
    {
        //$attr = strtoupper($attr);
        if (!isset($xml->$attr))
            return null;
        return (string)($xml->$attr);
    }
}