<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "people_proxy".
 *
 * @property integer $id
 * @property string $sname
 * @property string $name
 * @property string $pname
 * @property string $relation
 * @property integer $typedoc
 * @property string $serdoc
 * @property string $numdoc
 * @property string $datedoc
 * @property string $tel1
 * @property string $tel2
 * @property string $doveren
 * @property string $contacts
 * @property string $uved
 * @property integer $peopleid
 * @property string $insdate
 */
class Proxy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people_proxy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sname', 'name', 'pname', 'relation', 'serdoc', 'numdoc', 'datedoc', 'tel1', 'tel2', 'doveren', 'contacts', 'uved'], 'string'],
            [['typedoc', 'peopleid'], 'integer'],
            [['insdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sname' => 'Фамилия',
            'name' => 'Имя',
            'pname' => 'Отчество',
            'relation' => 'Отношение',
            'typedoc' => 'Тип документа',
            'serdoc' => 'Серия документа',
            'numdoc' => 'Номер документа',
            'datedoc' => 'Дата выдачи',
            'tel1' => 'Телефон',
            'tel2' => 'Телефон доп.',
            'doveren' => 'Доверенность',
            'contacts' => 'Контакты',
            'uved' => 'Уведомление',
            'peopleid' => 'Peopleid',
            'insdate' => 'Дата добавления',
        ];
    }
}
