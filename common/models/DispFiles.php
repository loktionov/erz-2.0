<?php

namespace common\models;

use common\components\ConsoleActiveRecord;
use common\components\MyHelper;
use common\components\XmlConstruct;
use yii;
use yii\db\ActiveQuery;
use yii\db\Query;
use ZipArchive;

/**
 * This is the model class for table "disp_files".
 *
 * @property integer $id
 * @property string $filename
 * @property string $mo
 * @property integer $y
 * @property integer $n
 * @property string $result
 * @property string $insdate
 * @property DispContent[] $content
 * @property DispIdent[] $ident
 * @property DispFlk[] $flk
 * @property DispZglv $zglv
 * @property string $type
 */
class DispFiles extends ConsoleActiveRecord
{
    public $fileNameRX = '/^(GT|GK)(\d{6})_(\d\d)(\d\d){0,1}(\d\d)\.XML$/i';
    public $OMSNameRX = '/^(GT|GK)(\d{6})_(\d\d)(\d\d){0,1}(\d\d)\.OMS$/i';
    const SCENARIO_ERROR = 'error';
    public $path;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if (!empty($this->path)) {

            $this->attrsFromOMS();

            if (!$this->hasErrors()) {
                $this->extractOMS();
            }
        }

    }

    public function extractOMS()
    {
        $zip = new ZipArchive;
        if ($zip->open($this->path) === TRUE) {
            $xmlname = $zip->getNameIndex(0);
            $this->filename = $xmlname;
            if (!preg_match($this->fileNameRX, $xmlname) OR basename(mb_strtoupper($this->path), 'OMS') != basename(mb_strtoupper($xmlname), 'XML')) {
                if (preg_match('/(26\d{4})/', $this->filename, $m)) {
                    $this->mo = $m[1];
                }
                $zip->close();
                $e = 'Некорректное содержимое архива';
                $this->addError('filename', $e);
                $this->errorsEx['filename']['zip'] = $e;
                return false;
            }

            $zip->extractTo('upload\in\extract\\', [$xmlname]);
            $this->path = 'upload\in\extract\\' . $xmlname;
            $zip->close();
            return true;
        } else {
            $e = 'Файл не является корректным zip-архивом';
            $this->addError('filename', $e);
            $this->errorsEx['filename']['zip'] = $e;
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename'], 'match', 'pattern' => $this->fileNameRX, 'message' => 'Имя файла не соответствует маске: GTNi_YYMMNN_Si.XML'],
            [['path'], 'fileExist'],
            [['filename', 'mo', 'y', 'n'], 'required'],
            [['mo', 'y'], 'fileNameCheck'],
            [['filename', 'mo'], 'string'],
            [['y', 'n'], 'integer'],
            [['insdate'], 'safe'],
            [['content'], 'validXML'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ERROR] = ['filename', 'result'];
        return $scenarios;
    }

    public function getFlk()
    {
        return $this->hasMany(DispFlk::className(), ['fid' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getContent()
    {
        return $this->hasMany(DispContent::className(), ['fid' => 'id']);
    }

    public function getZglv()
    {
        return $this->hasOne(DispZglv::className(), ['fid' => 'id']);
    }

    public function validXML()
    {
        $error = "Содержимое архива не является корректным XML-файлом." . PHP_EOL;
        //echo '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
        $error .= implode(PHP_EOL, $this->libxml_display_errors());
        $this->addError('file_xml', $error);
        return false;
    }

    public function fileExist()
    {
        if (!file_exists($this->path))
            $this->addError('filename', 'Не найден указанный файл');
    }

    public function fileNameCheck()
    {
        if (!MoBase::findOne($this->mo))
            $this->addError('mo', 'Не найден код МО');
        if (!in_array($this->y, $this->getYears()))
            $this->addError('y', 'Неверно указан год');
    }

    private function getYears()
    {
        return array(
            date('y', strtotime(\Yii::$app->params['disp_date'])),
        );
    }

    public function doublesCheck()
    {
        $rows = (new Query())
            ->select(['id_pac', 'count(*) c'])
            ->from('disp_content')
            ->where(['fid' => $this->id])
            ->groupBy('id_pac')
            ->having('count(*)>1')
            ->all();
        if (!empty($rows)) {
            $this->addError('filename', 'id_pac встречается более одного раза');
            foreach ($rows as $r) {
                $flk = new DispFlk(['scenario' => DispFlk::SCENARIO_LINK]);
                $flk->attr = 'id_pac';
                $flk->strict = DispFlk::STRICT_ERROR;
                $flk->ecode = 'id_pac_double';
                $flk->emess = 'id_pac=' . $r['id_pac'] . ' встречается ' . $r['c']
                    . ' ' . MyHelper::getNumEnding((int)$r['c'], ['раз', 'раза', 'раз']);
                $flk->id_pac = $r['id_pac'];
                $this->link('flk', $flk);
            }
        }
    }

    public function countCheck()
    {
        $erz = $this->getContentCount();
        $mapoms = DispMapomsView::find()->where(['kod' => $this->mo])->one();
        $mapoms = empty($mapoms) ? 0 : $mapoms->mapoms;
        $per = $erz / $mapoms * 100;
        if($per < 100 OR $per > 130){
            $this->errorsEx['content']['count'] = "Количество переданных записей не соответствует планам.";
        }

    }

    public function doublesCheckIdent()
    {
        $rows = (new Query())
            ->select(['i.pid', 'count(*) c'])
            ->from('disp_ident i')
            ->join('join', 'disp_content c', "c.id = i.cid")
            ->where(['c.fid' => $this->id])
            ->groupBy('pid')
            ->having('count(*)>1')
            ->all();
        if (!empty($rows)) {
            $this->addError('filename', 'ЗЛ встречается в файле более одного раза');
            foreach ($rows as $r) {
                $zl = (new Query())
                    ->select(['id_pac'])
                    ->from('disp_ident i')
                    ->join('join', 'disp_content c', "c.id = i.cid")
                    ->where(['c.fid' => $this->id, 'i.pid' => $r['pid']])
                    ->all();
                $z = [];
                foreach ($zl as $l) {
                    $z[] = $l['id_pac'];
                }
                if (empty($z))
                    continue;

                $id_pac = mb_substr(implode(', ', $z), 0, 50, 'UTF-8');
                $flk = new DispFlk(['scenario' => DispFlk::SCENARIO_LINK]);
                $flk->attr = 'id_pac';
                $flk->strict = DispFlk::STRICT_ERROR;
                $flk->ecode = 'id_pac_double';
                $flk->emess = 'id_pac=' . $id_pac . ' Указывает на одно и то же застрахованное лицо';
                $flk->id_pac = $id_pac;
                $this->link('flk', $flk);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Имя файла',
            'mo' => 'Код МО',
            'y' => 'Год',
            'n' => 'Номер',
            'result' => 'Результат',
            'insdate' => 'Дата файла',
            'type' => 'Тип файла',
        ];
    }

    public function attrsFromOMS()
    {
        $name = basename($this->path);
        $this->filename = $name;
        if (preg_match($this->OMSNameRX, $name, $m)) {
            $this->type = $m[1];
            $this->mo = $m[2];
            $this->y = $m[3];
            $this->n = $m[4];
            return;
        } else if (preg_match('/(GT|GK)(26\d{4})_{0,1}(\d\d){0,1}(\d\d){0,1}(\d\d){0,1}/', $name, $m)) {
            $this->type = $m[1];
            $this->mo = $m[2];
            $this->y = @$m[3];
            $this->n = @$m[4];
        }
        $e = 'Имя файла не соответствует маске';
        $this->addError('path', $e);
        $this->errorsEx['filename']['path'] = $e;
    }

    public function linkFlk($errors = [])
    {
        if (empty($errors))
            $errors = $this->errorsEx;
        foreach ($errors as $a => $err) {
            $flk = new DispFlk(['scenario' => self::SCENARIO_LINK]);
            $flk->attr = $a;
            $flk->strict = DispFlk::STRICT_ERROR;
            $flk->ecode = key($err);
            $flk->emess = array_shift($err);
            $this->link('flk', $flk);
        }
    }

    public function identification()
    {
        foreach ($this->getContent()->all() as $c) {
            $c->identification();
        }
    }

    public function stickmo()
    {
        $query = "INSERT INTO disp_flk (fid, strict, attr, ecode, emess, id_pac)
  SELECT f.id, 'error', 'mo', 'not_stick', 'ЗЛ не прикреплен к данной МО', c.id_pac  FROM disp_files f
  JOIN disp_content c ON c.fid=f.id
  JOIN disp_ident i ON i.cid=c.id
  LEFT JOIN stickmo s ON s.peopleid=i.pid AND replace(s.MOCode, '261079', '260079')=f.mo
  WHERE f.id=:fid AND s.id IS NULL";
        yii::$app->db->createCommand($query, [":fid" => $this->id])->execute();
    }



    public function getContentCount()
    {
        $clean_content = \Yii::$app->db->createCommand("

        SELECT count(*) FROM disp_files f
        JOIN disp_content c ON c.fid=f.id
        JOIN disp_ident i ON i.cid=c.id
        LEFT JOIN disp_flk flk ON flk.fid=f.id
        WHERE f.id=:id AND flk.id IS NULL

        ", [':id' => $this->id])->queryScalar();
        if ($clean_content != $this->getContent()->count())
            return 0;
        return $clean_content;
    }

    public function getFlkXml()
    {
        $xml = new XmlConstruct('FLK_LIST');
        $zglv_part = [];
        $content_count = $this->getContentCount();
        $comment = empty($content_count) ? 'Следует исправить ошибки и направить файл повторно' : 'Ошибок нет, файл обработан';
        if (!empty($this->zglv)) {
            $zglv_part = [
                'CODE_MO' => $this->zglv->code_mo,
                'EMAIL' => $this->zglv->email,
            ];
        }
        $zglv = ['ZGLV' => array_merge([
            'VERSION' => DispZglv::CUR_VERSION,
            'DATE' => date('Y-m-d'),
            'FILENAME' => $this->filename,
            'ERR_COUNT' => $this->getFlk()->count(),
            'ZL_COUNT' => $content_count,
        ], $zglv_part)];
        $zglv['ZGLV']['COMENT'] = $comment;
        $xml->fromArray($zglv);
        foreach ($this->getFlk()->all() as $flk) {
            $a = $flk->attributes;
            unset($a['id']);
            unset($a['fid']);
            $xml->fromArray(['FLK' => $a]);
        }
        return $xml->getDocument();
    }

    public function getSMOList()
    {
        $smo_list = \Yii::$app->db->createCommand("

        SELECT DISTINCT i.smo FROM disp_files f
        JOIN disp_content c ON c.fid=f.id
        JOIN disp_ident i ON i.cid=c.id
        LEFT JOIN disp_flk flk ON flk.fid=f.id
        WHERE f.id=:id AND flk.id IS NULL

        ", [':id' => $this->id])->queryAll();
        return $smo_list;
    }

    /**
     * @param $smo
     * @return ActiveQuery
     */
    public function getZlBySmo($smo)
    {
        /*$content_list = \Yii::$app->db->createCommand("

        SELECT DISTINCT c.* FROM disp_files f
        JOIN disp_content c ON c.fid=f.id
        JOIN disp_ident i ON i.cid=c.id
        WHERE f.id=:id AND i.smo=:smo

        ", [':id' => $this->id, ':smo' => $smo])->queryAll();
        return $content_list;*/

        return DispContent::find()->join('join', 'disp_ident i', 'disp_content.id = i.cid')
            ->join('join', 'v016 v', 'disp_content.disp = v.code')
            ->join('join', 'stickmo mo', 'i.pid = mo.peopleid')
            ->where('disp_content.fid=:fid and i.smo=:smo and v.disp_type in (12,22) and (:year - year(disp_content.dr)) >= 21 and mo.MOCode=:mo',
                [':fid' => $this->id, ':smo' => $smo, ':year' => '20' . $this->y, ':mo' => $this->mo]);
    }

    public function getSMOXml($tmp_path)
    {
        $bath = 10000;
        $doc = [];
        $smo_list = $this->getSMOList();
        foreach ($smo_list as $k => $smo) {

            $smo = $smo['smo'];

            $tmp = tempnam($tmp_path, '');
            $xmlWriter = new \XMLWriter();
            $xmlWriter->openUri($tmp);
            $xmlWriter->setIndent(true);


            $xml = new XmlConstruct('ZL_LIST');
            $zl = $this->getZlBySmo($smo);
            $zglv = ['ZGLV' => [
                'VERSION' => DispZglv::CUR_VERSION,
                'DATE' => date('Y-m-d'),
                'FILENAME' => $this->filename,
                'CODE_MO' => $this->zglv->code_mo,
                'SMO' => $smo,
                'ZL_COUNT' => $zl->count(),
                'EMAIL' => $this->zglv->email,
            ],];
            $xml->fromArray($zglv);

            $i = 0;
            try {
                foreach ($zl->each(1000) as $z) {
                    $i++;
                    $a = $z->attributes;
                    unset($a['id']);
                    unset($a['fid']);
                    $a = array_change_key_case($a, CASE_UPPER);
                    $xml->fromArray(['PERS' => $a]);
                    if ($i % $bath == 0) {
                        $xmlWriter->writeRaw($xml->outputMemory(true));
                    }
                }
            } catch (\PDOException $e) {
                if ($e->getCode() != 'IMSSP')
                    throw $e;
            }
            $xmlWriter->writeRaw($xml->getDocument());
            unset($xml);
            $xmlWriter->endElement();
            $xmlWriter->endDocument();
            $xmlWriter->flush();
            $doc[$smo] = $tmp;
        }
        return $doc;
    }
}
