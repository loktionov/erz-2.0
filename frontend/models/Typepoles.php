<?php

namespace frontend\models;

use yii;

/**
 * This is the model class for table "typepoles".
 *
 * @property integer $id
 * @property string $name
 * @property string $fname
 *
 * @property Reestr[] $reestrs
 */
class Typepoles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typepoles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'fname'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'fname' => 'Fname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReestrs()
    {
        return $this->hasMany(Reestr::className(), ['typePoles' => 'id']);
    }
}
