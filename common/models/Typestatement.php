<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "typestatement".
 *
 * @property integer $id
 * @property string $Name
 * @property string $Comment
 */
class Typestatement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typestatement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'Comment' => 'Comment',
        ];
    }
}
