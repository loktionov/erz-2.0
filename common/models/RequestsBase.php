<?php

namespace common\models;

use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "requests".
 *
 * @property integer $rid
 * @property integer $pid
 * @property string $enp
 * @property string $p
 * @property string $stmdate
 * @property string $serdoc
 * @property string $numdoc
 * @property string $typedoc
 * @property string $sname
 * @property string $name
 * @property string $pname
 * @property string $dateman
 * @property string $datedeath
 * @property integer $sexman
 * @property string $pbman
 * @property string $okato
 * @property string $snils
 * @property string $prev_ogrn
 * @property string $prev_datebegin
 * @property string $prev_dateend
 * @property string $prev_okato
 * @property string $prev_sname
 * @property string $prev_name
 * @property string $prev_pname
 * @property string $prev_dateman
 * @property string $prev_typepoles
 * @property string $prev_enumber
 * @property integer $prev_sexman
 * @property string $prev_enp
 * @property string $prev_serdoc
 * @property string $prev_numdoc
 * @property string $prev_typedoc
 * @property string $ogrn
 * @property string $begindate
 * @property string $dateend
 * @property string $typepoles
 * @property string $enumber
 * @property string $uprak1code
 * @property string $uprak1str
 * @property string $uprak2code
 * @property string $uprak2str
 * @property string $appak1code
 * @property string $appak1str
 * @property string $appak2code
 * @property string $appak2str
 * @property integer $pvcode
 * @property integer $packnumber
 * @property integer $prev_packnumber
 * @property string $insdate
 * @property string $update_time
 * @property integer $req_status
 * @property integer $reasong
 * @property integer $orgid
 * @property string $nationman
 * @property string $datedoc
 * @property string $doc_date_end
 * @property string $prev_snils
 * @property string $prev_pbman
 * @property string $id_source
 *
 * @property RequestsBookmark[] $requestsBookmarks
 */
class RequestsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'sexman', 'prev_sexman', 'pvcode', 'packnumber', 'prev_packnumber', 'req_status', 'reasong', 'orgid'], 'integer'],
            [['enp', 'p', 'serdoc', 'numdoc', 'typedoc', 'sname', 'name', 'pname', 'pbman', 'okato', 'snils', 'prev_ogrn',
                'prev_okato', 'prev_sname', 'prev_name', 'prev_pname', 'prev_typepoles', 'prev_enumber', 'prev_enp', 
                'prev_serdoc', 'prev_numdoc', 'prev_typedoc', 'ogrn', 'typepoles', 'enumber', 'uprak1code', 'uprak1str', 
                'uprak2code', 'uprak2str', 'appak1code', 'appak1str', 'appak2code', 'appak2str', 'nationman', 'prev_snils', 
                'prev_pbman', 'id_source'], 'string'],
            [['p', 'pid'], 'required'],
            [['stmdate', 'dateman', 'datedeath', 'prev_datebegin', 'prev_dateend', 'prev_dateman', 'begindate', 
                'dateend', 'insdate', 'update_time', 'datedoc', 'doc_date_end', 'id_source'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rid' => 'Rid',
            'pid' => 'Pid',
            'enp' => 'Enp',
            'p' => 'P',
            'stmdate' => 'Stmdate',
            'serdoc' => 'Serdoc',
            'numdoc' => 'Numdoc',
            'typedoc' => 'Typedoc',
            'sname' => 'Sname',
            'name' => 'Name',
            'pname' => 'Pname',
            'dateman' => 'Dateman',
            'datedeath' => 'Datedeath',
            'sexman' => 'Sexman',
            'pbman' => 'Pbman',
            'okato' => 'Okato',
            'snils' => 'Snils',
            'prev_ogrn' => 'Prev Ogrn',
            'prev_datebegin' => 'Prev Datebegin',
            'prev_dateend' => 'Prev Dateend',
            'prev_okato' => 'Prev Okato',
            'prev_sname' => 'Prev Sname',
            'prev_name' => 'Prev Name',
            'prev_pname' => 'Prev Pname',
            'prev_dateman' => 'Prev Dateman',
            'prev_typepoles' => 'Prev Typepoles',
            'prev_enumber' => 'Prev Enumber',
            'prev_sexman' => 'Prev Sexman',
            'prev_enp' => 'Prev Enp',
            'prev_serdoc' => 'Prev Serdoc',
            'prev_numdoc' => 'Prev Numdoc',
            'prev_typedoc' => 'Prev Typedoc',
            'ogrn' => 'Ogrn',
            'begindate' => 'Begindate',
            'dateend' => 'Dateend',
            'typepoles' => 'Typepoles',
            'enumber' => 'Enumber',
            'uprak1code' => 'Uprak1code',
            'uprak1str' => 'Uprak1str',
            'uprak2code' => 'Uprak2code',
            'uprak2str' => 'Uprak2str',
            'appak1code' => 'Appak1code',
            'appak1str' => 'Appak1str',
            'appak2code' => 'Appak2code',
            'appak2str' => 'Appak2str',
            'pvcode' => 'Pvcode',
            'packnumber' => 'Packnumber',
            'prev_packnumber' => 'Prev Packnumber',
            'insdate' => 'Insdate',
            'update_time' => 'Update Time',
            'req_status' => 'Req Status',
            'reasong' => 'Reasong',
            'orgid' => 'Orgid',
            'nationman' => 'Nationman',
            'datedoc' => 'Datedoc',
            'doc_date_end' => 'Doc Date End',
            'prev_snils' => 'Prev Snils',
            'prev_pbman' => 'Prev Pbman',
            'id_source' => 'id_source',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestsBookmarks()
    {
        return $this->hasMany(RequestsBookmark::className(), ['rid' => 'rid']);
    }
}
