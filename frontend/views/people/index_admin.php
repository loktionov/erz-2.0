<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PeopleSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'action' => ['index_admin'],
    'method' => 'get',
]); ?>
<?= $this->render('_search', [
    'model' => $model,
    'form' => $form,
]) ?>

<?php ActiveForm::end(); ?>

<div class="people-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ENP',
            'Name',
            'sName',
            'pName',
            // 'dateMan',
            // 'sexMan',
            // 'pbMan',
            // 'nationMan',
            // 'typeDoc',
            // 'serDoc',
            // 'numDoc',
            // 'dateDoc',
            // 'statusMan',
            // 'regKladrId',
            // 'addressReg',
            // 'dateReg',
            // 'addressLive',
            // 'activPolesId',
            // 'dateDeath',
            // 'liveKladrId',
            // 'snils',
            // 'okato_mj',
            // 'InsDate',
            // 'cr',
            // 'crdate',
            // 'lostman',
            // 'proxy_man',
            // 'contact',
            // 'doc_date_end',
            // 'livetemp',
            // 'refugee',
            // 'orgdoc',
            // 'FFOMS',
            // 'wid',
            // 'okato_reg',
            // 'kladr_reg',
            // 'kladr_live',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
