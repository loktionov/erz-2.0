<?php

namespace console\controllers;


use common\models\ReqDoc;
use console\models\RequestsConsole, yii\console\Controller;
use console\models\RequestsQBP;

class RequestController extends Controller
{

    public function actionZp1($p = 'zp1_misc')
    {
        $r = RequestsQBP::find()->with(['pers', 'doc', 'polis'])->where('rid=:rid', [':rid' => 41750343])->all();
        $r[0]->getXmlBody();
        $d = new ReqDoc();
        $r[0]->link('doc', $d);
        $docs = $r->doc;
        foreach ($docs as $doc)
            $r->unlink('doc', $doc, true);

        //echo RequestsConsole::createQBP_ZP1(232555);
    }

    public function actionIndex()
    {
        echo 'ok';
    }
}