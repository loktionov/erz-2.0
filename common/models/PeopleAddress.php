<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "people_address".
 *
 * @property integer $id
 * @property integer $peopleid
 * @property string $addr_type
 * @property string $okato
 * @property string $kladr
 * @property string $addr_full
 * @property string $house
 * @property string $building
 * @property string $apartment
 * @property string $unknown_street
 * @property integer $actual
 * @property string $insdate
 * @property string $regdate
 * @property string $fias
 */
class PeopleAddress extends \yii\db\ActiveRecord
{
    public $parents;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['peopleid', 'addr_type'], 'required'],
            [['peopleid', 'actual'], 'integer'],
            [['addr_type', 'okato', 'kladr', 'addr_full', 'house', 'building', 'apartment', 'unknown_street', 'fias'], 'string'],
            [['insdate', 'regdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'peopleid' => 'Peopleid',
            'addr_type' => 'Тип адреса',
            'okato' => 'ОКАТО',
            'kladr' => 'КЛАДР',
            'addr_full' => 'Полный адрес',
            'house' => 'Дом',
            'building' => 'Строение',
            'apartment' => 'Квартира',
            'unknown_street' => 'Неизвестный адрес',
            'actual' => 'Актуальность',
            'insdate' => 'Дата добавления',
            'regdate' => 'Дата регистрации',
            'fias' => 'ФИАС',
        ];
    }

    public static function getAddress($pid, $type, $okato, $kladr, $addr_full)
    {
        $unknown_street = '';
        $house = '';
        $building = '';
        $apartment = '';
        $addr = explode(';', $addr_full);
        if (!empty($kladr) and !empty($addr_full)
            //and in_array(count($addr), array(1, 2))cls
        ) {
            $addr_part = $addr[0];
            $house_part = '';
            if (count($addr) == 2) {
                $house_part = $addr[1];
            }
            $unknown_rx = '/\|(.+)\|/iu';
            if (preg_match($unknown_rx, $addr_part, $m)) {
                $unknown_street = $m[1];
            }
            $house_rx = '/д\.\s*(.+?)(,|$)/ui';
            if (preg_match($house_rx, $house_part, $h)) {
                $house = $h[1];
            }
            $building_rx = '/к\.\s*(.+?)(,|$)/ui';
            if (preg_match($building_rx, $house_part, $b)) {
                $building = $b[1];
            }
            $apartmen_rx = '/кв\.\s*(.+)\s*$/ui';
            if (preg_match($apartmen_rx, $house_part, $a)) {
                $apartment = $a[1];
            }
        }
        return array(
            'peopleid' => $pid,
            'addr_type' => $type,
            'okato' => $okato,
            'kladr' => $kladr,
            'addr_full' => $addr_full,
            'house' => $house,
            'building' => $building,
            'apartment' => $apartment,
            'unknown_street' => $unknown_street,
            'actual' => 1
        );
    }

    public function getAddressByKladr()
    {
        $q = "
WITH child_to_parents AS (
  SELECT addrobj.* FROM fias.dbo.addrobj
      WHERE code = :kladr
  UNION ALL
  SELECT addrobj.* FROM fias.dbo.addrobj, child_to_parents
      WHERE addrobj.aoguid = child_to_parents.parentguid
        AND addrobj.currstatus = 0
)
SELECT c.AOLEVEL, c.OFFNAME, c.SHORTNAME, c.POSTALCODE
FROM child_to_parents c 
ORDER BY aolevel;";

        $addr = Yii::$app->db->createCommand($q, [':kladr' => $this->kladr])->queryAll();

        $region = '';
        $strict = '';
        $city = '';
        $locality = '';
        $street = '';
        $postal_code = '';

        foreach ($addr as $a) {
            switch ($a['AOLEVEL']) {
                case 1:
                    $region .= $a['OFFNAME'] . ' ' . $a['SHORTNAME'];
                    break;
                case 3:
                    $strict .= $a['OFFNAME'] . ' ' . $a['SHORTNAME'];
                    break;
                case 4:
                    $city .= $a['SHORTNAME'] . ' ' . $a['OFFNAME'];
                    break;
                case 5:
                case 6:
                    $locality .= $a['SHORTNAME'] . ' ' . $a['OFFNAME'];
                    break;
                case 7:
                case 90:
                case 91:
                    $street .= $a['SHORTNAME'] . ' ' . $a['OFFNAME'];
                    $postal_code = $a['POSTALCODE'];
                    break;
                default:
                    break;
            }
        }

        return [
            self::REGION => $region,
            self::STRICT => $strict,
            self::CITY => $city,
            self::LOCALITY => $locality,
            self::STREET => $street,
            self::POSTAL_CODE => $postal_code,
        ];

    }

    public function setFiasByKladr()
    {
        $q = "SELECT addrobj.AOGUID FROM fias.dbo.addrobj
      WHERE code = :kladr AND addrobj.currstatus = 0;";

        $addr = Yii::$app->db->createCommand($q, [':kladr' => $this->kladr])->queryOne();
        $this->fias = ($addr) ? $addr['AOGUID'] : null;
    }

    public function getAddressCodeByKladr()
    {
        $q = "
WITH child_to_parents AS (
  SELECT addrobj.* FROM fias.dbo.addrobj
      WHERE code = :kladr
  UNION ALL
  SELECT addrobj.* FROM fias.dbo.addrobj, child_to_parents
      WHERE addrobj.aoguid = child_to_parents.parentguid
        AND addrobj.currstatus = 0
)
SELECT c.AOGUID, c.AOLEVEL, c.POSTALCODE
FROM child_to_parents c 
ORDER BY aolevel;";

        $addr = Yii::$app->db->createCommand($q, [':kladr' => $this->kladr])->queryAll();

        $region = '327a060b-878c-4fb4-8dc4-d5595871a3d8';
        $strict = '';
        $city = '';
        $locality = '';
        $street = '';
        $postal_code = '';

        foreach ($addr as $a) {
            switch ($a['AOLEVEL']) {
                case 1:
                    $region = $a['AOGUID'];
                    break;
                case 3:
                    $strict = $a['AOGUID'];
                    break;
                case 4:
                    $city = $a['AOGUID'];
                    break;
                case 5:
                case 6:
                    $locality = $a['AOGUID'];
                    break;
                case 7:
                case 90:
                case 91:
                    $street = $a['AOGUID'];
                    $postal_code = $a['POSTALCODE'];
                    break;
                default:
                    break;
            }
        }

        $strict = (empty($strict) AND !empty($city)) ? $city : $strict;
        $locality = (empty($locality) AND !empty($city)) ? $city : $locality;

        $parents = [
            self::REGION => $region,
            self::STRICT => $strict,
            self::CITY => $city,
            self::LOCALITY => $locality,
            self::STREET => $street,
            self::POSTAL_CODE => $postal_code,
        ];
        $this->parents = $parents;
        return $parents;
    }

    const REGION = 'region';
    const STRICT = 'strict';
    const CITY = 'city';
    const LOCALITY = 'locality';
    const STREET = 'street';
    const POSTAL_CODE = 'postal_code';

    public static function getLvlArray($ref)
    {
        $res = [];
        if (is_array($ref)) {
            foreach ($ref as $r) {
                $res = array_merge($res, self::getLvlArray($r));
            }
            return $res;
        }
        switch ($ref) {
            case self::REGION:
                return [1];
            case self::STRICT:
                return [3];
            case self::CITY:
                return [4];
            case self::LOCALITY:
                return [5, 6];
            case self::STREET:
                return [7, 90, 91];
            default:
                return [];
        }
    }

    /**
     * @param $reg self
     * @param $live self
     * @return bool
     */
    public static function isEqual($reg, $live)
    {
        if ($reg->kladr != $live->kladr)
            return false;
        if ($reg->house != $live->house)
            return false;
        if ($reg->building != $live->building)
            return false;
        if ($reg->apartment != $live->apartment)
            return false;
        return true;
    }

}
