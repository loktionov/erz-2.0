<?php
/**
 * @var $dataProvider \common\models\DispFiles
 * @var $searchModel \common\models\DispFilesSearch
 * @var $this \yii\web\View
 *
 *
 */
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/*echo \yii\helpers\Html::a(
    'Получить аякс', '#', [ 'id' => 'test' ]
);*/
//Pjax::begin(['id' => 'admin-crud-id', 'timeout' => false,]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id', 'filename', 'mo', 'y', 'n', 'result', 'insdate',
        [
            'header' => "ФЛК",
            'format' => 'raw',
            'value' => function ($data) {
                $flk = $data->getFlk()->count();
                if (empty($flk))
                    return 0;
                return Html::a($flk, '#', ['data-toggle' => 'modal', 'data-target' => '#flk-modal', 'data-fileid' => $data->id]);
            }
        ],
        [
            'header' => "Контент",
            'format' => 'raw',
            'value' => function ($data) {
                return $data->getContent()->count();
            }
        ],
    ]
]); ?>
<div id="flk-modal" class="modal fade bs-example-modal-lg" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>
<? //Pjax::end();
$js = <<<JS
$(document).ready(function () {
    $('#flk-modal').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body').html('');
        var button = $(event.relatedTarget); // Button that triggered the modal
        var id = button.data('fileid'); // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  
   $.ajax({
                url:'/disp/flk?fid=' + id,                
                success: function(data) {
                  modal.find('.modal-body').html(data);
                }
            });
    
})
        
    });
JS;

$this->registerJs($js); ?>
