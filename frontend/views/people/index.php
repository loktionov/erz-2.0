<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\People */
/* @var $modelSearch frontend\models\PeopleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peoples';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>
<?= $this->render('_search', [
    'model' => $modelSearch,
    'form' => $form,
]) ?>

<?php ActiveForm::end(); ?>

<?= $this->render('_view', [
    'model' => $model,
]) ?>