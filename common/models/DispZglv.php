<?php

namespace common\models;

use common\components\ConsoleActiveRecord;
use common\components\EmailsValidator;
use Yii;

/**
 * This is the model class for table "disp_zglv".
 *
 * @property integer $id
 * @property integer $fid
 * @property string $vers
 * @property string $fdate
 * @property string $filename
 * @property string $code_mo
 * @property string $email
 * @property string $comment
 * @property DispFiles $file
 */
class DispZglv extends ConsoleActiveRecord
{
    const CUR_VERSION = '2.1';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_zglv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vers', 'filename', 'code_mo', 'email', 'comment', 'fdate'], 'trim'],
            [['fid'], 'required'],
            [['fid'], 'integer'],
            [['vers', 'filename', 'code_mo', 'email', 'comment'], 'string'],
            [['fdate'], 'safe'],
            [['email'], EmailsValidator::className()],
            [['vers'], 'Version'],
            [['code_mo', 'filename'], 'file_check'],
        ];
    }

    public function file_check($a){
        if(empty($this->fid))
            return;
        $file = $this->file;
        $fa = str_replace('code_','',$a);
        $rx = '/\.\w+$/';
        if(preg_replace($rx, '', mb_strtoupper($file->$fa)) != preg_replace($rx, '', mb_strtoupper($this->$a))){
            $this->addError($a, $this->getAttributeLabel($a) . ' в заголовке документа не соответствует имени архива.');
        }
    }

    public function Version(){
        if($this->vers != self::CUR_VERSION)
            $this->addError('vers', 'Версия файла не соответствует текущей');
    }

    public function getFile()
    {
        return $this->hasOne(DispFiles::className(), ['id' => 'fid']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LINK] = ['vers', 'filename', 'code_mo', 'email', 'comment', 'fdate'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fid' => 'Fid',
            'vers' => 'Версия',
            'fdate' => 'Дата файла',
            'filename' => 'Имя файла',
            'code_mo' => 'Код МО',
            'email' => 'E-mail',
            'comment' => 'Комментарий',
        ];
    }

    public function saveFlk($errors = [])
    {
        if (empty($errors))
            $errors = $this->errorsEx;
        foreach ($errors as $a => $err) {
            $flk = new DispFlk(['scenario' => self::SCENARIO_LINK]);
            $flk->attr = $a;
            $flk->strict = DispFlk::STRICT_ERROR;
            $flk->ecode = key($err);
            $flk->emess = array_shift($err);
            $flk->fid = $this->fid;
            $flk->save(false);
        }
    }
}
