<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use \yii\helpers\ArrayHelper;
use dosamigos\selectize\SelectizeDropDownList;

use yii\widgets\MaskedInput;


/* @var $this yii\web\View */
/* @var $model common\models\StickMO */
/* @var $form yii\widgets\ActiveForm */
?>
<script>
    function RenderOptionWithOpt(item, escape) {
        var label = item.text;
        var cat = item.cat ? '<br /><span class="small" style="color: grey">' + item.cat + '</span>' : '';
        return '<div>' + escape(label) + cat + '</div>';
    }

    function getChildList(url, child_selector) {
        var sel = $(child_selector);
        sel.html('');
        $.post(url, function (data) {
            $.each(data, function (i, d) {
                var opt = $('<option></option>').val(i).html(d);
                sel.append(opt);
            });
            sel.trigger("change");
        }, 'json');
    }
</script>

<div class="stick-mo-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'peopleid')->hiddenInput()->label(false) ?>

    <?php echo $form->field($model, "okato")->dropDownList(
        ArrayHelper::map(\frontend\models\OKATO::find()
            ->where("OKATO <> '0'")
            ->orderBy(['Name' => "asc"])
            ->all(), 'OKATO', 'Name'), [
            'prompt' => 'Выберите регион',
            'span' => 5,
            'onchange' => '
               getChildList("' . Yii::$app->urlManager->createAbsoluteUrl('stickmo/getmo?okato=') . '"+$(this).val(),"select#stickmo-mocode");
            ',
        ]
    ); ?>


    <?= '' //Html::activeLabel($model,'PodMOCode');      ?>
    <?php echo $form->field($model, 'MOCode')->dropDownList(ArrayHelper::map(\frontend\models\MO::find()
        ->where("mo_type is not null")
        ->orderBy(['mo_type' => 'desc', 'NAMMO' => 'asc'])
        ->all(), 'KODMO', 'NAMMO'), [
            'prompt' => 'Выберите медицинскую организацию',
            'span' => 5,
            'onchange' => 'getChildList( "' . Yii::$app->urlManager->createAbsoluteUrl('stickmo/getpmo?kodmo=') . '"+$(this).val(), "select#stickmo-podmocode")',

        ]
    ); ?>

    <?php echo $form->field($model, 'PodMOCode')->dropDownList(ArrayHelper::map(\frontend\models\PMO::find()
        ->where('KODMO=:MO', [':MO' => $model->MOCode])
        ->orderBy('NAMPMO')
        ->all(), 'KODPMO', 'NAMPMO'),
        ['span' => 5, 'prompt' => 'Выберите подразделение',]); ?>
    <?=
    '' /*SelectizeDropDownList::widget([
        'model' => $model,
        'attribute' => 'PodMOCode',
        'items'=>PMO::GetSelectizeOption($model->PodMOCode) ?: [],
        'clientOptions' => [
            'options' => PMO::GetSelectizeArray(),
            'sortField' => 'text',
            'searchField' => array('text', 'cat'),
            'create' => false,
            'render' => array(
                'option' => new \yii\web\JsExpression('function(item, escape) { return RenderOptionWithOpt(item, escape); }'),
            ),
        ],
    ]);*/
    ?>


    <?=
    'Дата начала' . DatePicker::widget([
        'model' => $model,
        'attribute' => 'DateBegin',
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]); ?>

    <?=
    'Дата окончания' . DatePicker::widget([
        'model' => $model,
        'attribute' => 'DateEnd',
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]); ?>

    <?= $form->field($model, 'Reason')->dropDownList(ArrayHelper::map(\frontend\models\RefStickReasons::find()->all(), 'id', 'ReasonName')) ?>

    <?= $form->field($model, 'UchCode')->dropDownList(ArrayHelper::map(\frontend\models\RefUchastok::find()->all(), 'id', 'Name')) ?>


    <? echo Html::activeLabel($model,'DocCode'); ?>
    <? echo MaskedInput::widget([
        'model' => $model,
        'attribute' => 'DocCode',
        'mask' => '999-999-999 99',
    ]); ?>


    <?=
    'Дата прикрепления' . DatePicker::widget([
        'model' => $model,
        'attribute' => 'StickDate',
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Прикрепить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
