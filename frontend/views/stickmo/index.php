<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stick Mos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stick-mo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Stick Mo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'peopleid',
            'PodMOCode',
            'MOCode',
            'DateBegin',
            // 'DateEnd',
            // 'Reason',
            // 'UchCode',
            // 'DocCode',
            // 'StickDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
