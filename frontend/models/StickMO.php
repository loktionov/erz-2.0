<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 28.10.2016
 * Time: 9:31
 */

namespace frontend\models;

use common\components\MyHelper;
use common\models\StickMOBase;
use yii\helpers\ArrayHelper;

/**
 * @property string $region
 * @property string $okato
 */
class StickMO extends StickMOBase
{

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['DocCode'], 'validateSnils']
        ]);
    }

    public function validateSnils($attribute, $params)
    {
        if (!MyHelper::checkSNILS($this->$attribute))
            $this->addError($attribute, "Ошибка в СНИЛСе врача");
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stickmo_view';
    }

    public function getMo()
    {
        return $this->hasOne(MO::className(), ["KODMO" => "MOCode"]);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($runValidation AND !$this->validate())
            return false;
        if ($this->isNewRecord)
            $s = new StickMOBase();
        else
            $s = StickMOBase::findOne(['id' => $this->id]);

        if (substr($this->MOCode, 0, 2) != '26')
            $this->Reason = 4;
        $s->setAttributes($this->attributes);
        if ($s->save($runValidation, $attributeNames))
            return true;
        $this->addErrors($s->errors);
        return false;
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'peopleid' => 'Peopleid',
            'PodMOCode' => 'Подразделение МО',
            'MOCode' => 'Мед. организация',
            'DateBegin' => 'Дата начала',
            'DateEnd' => 'Дата окончания',
            'Reason' => 'Причина',
            'UchCode' => 'Код участка',
            'DocCode' => 'СНИЛС врача',
            'StickDate' => 'Дата прикрепления',
            'file_name' => 'File Name',
            'send_date' => 'Send Date',
            'snils_correct' => 'Snils Correct',
            'doc_correction' => 'Doc Correction',
            'okato' => 'Регион'
        ];
    }

}