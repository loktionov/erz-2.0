<?php

/**
 * @var $type string
 * @var $model PeopleAddress
 * @var $form ActiveForm
 */

use common\models\PeopleAddress;
use common\models\Fiasaddrobj;
use common\models\Address;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="form-group">
    <div class="pull-left select2-container">
        <?= Select2::widget([
            'name' => "PeopleAddress[$type][region]",
            'id' => "region_$type",
            'value' => $model->parents['region'],
            'data' => Fiasaddrobj::getRegions(),
            'options' => ['multiple' => false, 'placeholder' => 'Регион',],
            'pluginOptions' => ['allowClear' => true],

        ]); ?>
    </div>
</div>
<div class="form-group">
    <div class="pull-left select2-container">
        <?= DepDrop::widget([
            'type' => DepDrop::TYPE_SELECT2,
            'name' => "PeopleAddress[$type][city]",
            'id' => "city_$type",
            'value' => empty($model->parents['strict']) ? $model->parents['city'] : $model->parents['strict'],
            'data' => Fiasaddrobj::getObjsByParent($model->parents['region'], [PeopleAddress::CITY, PeopleAddress::STRICT]),
            'options' => ['multiple' => false, 'placeholder' => 'Район'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ["region_$type"],
                'placeholder' => 'Район',
                'url' => Url::to(['/people/address_child'])
            ]
        ]); ?>
    </div>
</div>
<div class="form-group">
    <div class="pull-left select2-container">
        <?= DepDrop::widget([
            'type' => DepDrop::TYPE_SELECT2,
            'name' => "PeopleAddress[$type][city2]",
            'id' => "city2_$type",
            'value' => $model->parents['locality'],
            'data' => Fiasaddrobj::getObjsByParent(empty($model->parents['strict']) ? $model->parents['city'] : $model->parents['strict'], PeopleAddress::LOCALITY),
            'options' => ['multiple' => false, 'placeholder' => 'Населенный пункт'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ["city_$type"],
                'placeholder' => 'Населенный пункт',
                'url' => Url::to(['/people/address_child', 'level' => 'city'])
            ]
        ]); ?>
    </div>
</div>
<div class="form-group">
    <div class="pull-left select2-container">
        <?= DepDrop::widget([
            'type' => DepDrop::TYPE_SELECT2,
            'name' => "PeopleAddress[$type][street]",
            'id' => "street_$type",
            'value' => $model->parents['street'],
            'data' => Fiasaddrobj::getObjsByParent($model->parents['locality'], PeopleAddress::STREET),
            'options' => ['multiple' => false, 'placeholder' => 'Улица'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ["city_$type", "city2_$type"],
                'placeholder' => 'Улица',
                'url' => Url::to(['/people/address_child', 'level' => 'street'])
            ]
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="cell sub-cell3"><?= $form->field($model, "[$type]house"); ?></div>
    <div class="cell sub-cell3"><?= $form->field($model, "[$type]building"); ?></div>
    <div class="cell sub-cell3"><?= $form->field($model, "[$type]apartment"); ?></div>
</div>