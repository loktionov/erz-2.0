<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property integer $userId
 * @property string $Name
 * @property string $sName
 * @property string $pName
 * @property string $Login
 * @property string $Password
 * @property integer $OrgId
 * @property string $UserHash
 * @property string $grname
 * @property string $insdate
 * @property string $annuldate
 * @property integer $pvcode
 * @property string $Birthday
 * @property integer $Sex
 * @property integer $Department
 * @property integer $Dolgnost
 * @property resource $Photo
 * @property string $Phone
 *
 * @property ManualRequests[] $manualRequests
 * @property RequestsBookmark[] $requestsBookmarks
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const ROLE_USER = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'sName', 'pName', 'Login', 'Password', 'UserHash', 'grname', 'Photo', 'Phone'], 'string'],
            [['Login', 'Password'], 'required'],
            [['OrgId', 'pvcode', 'Sex', 'Department', 'Dolgnost'], 'integer'],
            [['insdate', 'annuldate', 'Birthday'], 'safe'],
            [['Login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'Name' => 'Name',
            'sName' => 'S Name',
            'pName' => 'P Name',
            'Login' => 'Login',
            'Password' => 'Password',
            'OrgId' => 'Org ID',
            'UserHash' => 'User Hash',
            'grname' => 'Grname',
            'insdate' => 'Insdate',
            'annuldate' => 'Annuldate',
            'pvcode' => 'Pvcode',
            'Birthday' => 'Birthday',
            'Sex' => 'Sex',
            'Department' => 'Department',
            'Dolgnost' => 'Dolgnost',
            'Photo' => 'Photo',
            'Phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManualRequests()
    {
        return $this->hasMany(ManualRequests::className(), ['userid' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestsBookmarks()
    {
        return $this->hasMany(RequestsBookmark::className(), ['userid' => 'userId']);
    }

    public function getUsername(){
        return implode(' ', [$this->sName, $this->Name, $this->pName]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['Login' => $username, 'annuldate' => null]);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['userId' => $id, 'annuldate' => null]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $x = 0;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->userId;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return md5($this->userId);
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return md5(md5($password)) === trim($this->Password);
    }
}
