<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DispFlk;

/**
 * DispFlkSearch represents the model behind the search form about `common\models\DispFlk`.
 */
class DispFlkSearch extends DispFlk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fid'], 'integer'],
            [['strict', 'attr', 'ecode', 'emess', 'id_pac'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DispFlk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fid' => $this->fid,
        ]);

        $query->andFilterWhere(['like', 'strict', $this->strict])
            ->andFilterWhere(['like', 'attr', $this->attr])
            ->andFilterWhere(['like', 'ecode', $this->ecode])
            ->andFilterWhere(['like', 'emess', $this->emess])
            ->andFilterWhere(['like', 'id_pac', $this->id_pac]);

        return $dataProvider;
    }
}
