<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PeopleSearch */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="people-search" id="people">

    <?= $form->field($model, 'ENP')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '9999999999999999',
    ]) ?>

    <?= $form->field($model, 'polis') ?>

    <?= $form->field($model, 'sName') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'pName') ?>

    <?php echo $form->field($model, 'dateMan', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
        ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999'])?>

    <?php echo $form->field($model, 'typeDoc')->dropDownList(

        \common\models\Typedoc::find()->select(['Name', 'id'])->indexBy('id')->orderBy('Name')->column(),
        ['prompt' => 'Выберите документ']

    ) ?>

    <?php echo $form->field($model, 'serDoc') ?>

    <?php echo $form->field($model, 'numDoc') ?>

    <?php echo $form->field($model, 'snils')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999-999-999 99']) ?>

    <div class="form-group">
        <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Очистить', ['class' => 'btn btn-default']) ?>
    </div>