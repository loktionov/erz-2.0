<? /*= $form->field($model, 'proxy_man')->textInput() */ ?>
<div class="row">
    <div class="cell"><?= $form->field($proxy, 'sname')->textInput() ?></div>
    <div class="cell"><?= $form->field($proxy, 'name')->textInput() ?></div>
    <div class="cell"><?= $form->field($proxy, 'pname')->textInput() ?></div>
    <div class="cell"><?= $form->field($proxy, 'relation')->dropDownList([1 => "отец", 2 => "мать", 3 => "иное"], ['prompt' => ' ']) ?></div>
</div>
<div class="row">
    <div class="cell"><?= $form->field($proxy, 'typedoc')->dropDownList(

            \common\models\Typedoc::find()->select(['Name', 'id'])->indexBy('id')->orderBy('Name')->column(),
            ['prompt' => 'Выберите документ']

        ) ?></div>
    <div class="cell"><?= $form->field($proxy, 'serdoc')->textInput() ?></div>
    <div class="cell"><?= $form->field($proxy, 'numdoc')->textInput() ?></div>
    <div class="cell"><?= $form->field($proxy, 'datedoc')->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
            ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999']) ?></div>
</div>
<div class="row">
    <div class="cell">
        <?= $form->field($proxy, 'doveren')->dropDownList([0 => 'нет', 1 => 'да']) ?></div>
    <div class="cell"><?= $form->field($proxy, 'contacts')->textInput() ?></div>
</div>
