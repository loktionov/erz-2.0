<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\People */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Peoples', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Выдать полис', ['/polis/create', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите выдать новый полис?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'ENP',
            'Name',
            'sName',
            'pName',
            'dateMan',
            'sexMan',
            'pbMan',
            'nationMan',
            'typeDoc',
            'serDoc',
            'numDoc',
            'dateDoc',
            'statusMan',
            //'regKladrId',
            'addressReg',
            'dateReg',
            'addressLive',
            //'activPolesId',
            'dateDeath',
            //'liveKladrId',
            'snils',
            'okato_mj',
            'InsDate',
            //'cr',
            //'crdate',
            'lostman',
            //'proxy_man',
            'contact',
            'doc_date_end',
            //'livetemp',
            //'refugee',
            'orgdoc',
            //'FFOMS',
           // 'wid',
            'okato_reg',
            'kladr_reg',
            'kladr_live',
        ],
    ]) ?>

</div>
