<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 15.12.2016
 * Time: 10:01
 */

namespace common\components;


use common\models\PeopleBase;
use DateTime;
use yii\base\Behavior;
use yii\db\Expression;

class EnpBehavior extends Behavior
{

    public static function GetENPFirstpart(DateTime $DateBirthday, $sexMan)
    {
        $d = $DateBirthday->format("d");
        $m = $DateBirthday->format("m");
        $y = $DateBirthday->format("Y");

        if ($y <= 1950)
            $m = (int)$m + 20;
        if ($y >= 1951 && $y <= 2000)
            $m = $m + 40;
        $m1 = 9 - substr($m, 0, 1);
        $m2 = 9 - substr($m, 1, 1);
        $m = $m1 . $m2;

        $y1 = 9 - substr($y, 0, 1);
        $y2 = 9 - substr($y, 1, 1);
        $y3 = 9 - substr($y, 2, 1);
        $y4 = 9 - substr($y, 3, 1);
        $y = $y4 . $y3 . $y2 . $y1;

        if ($sexMan == 1)
            $d = (int)$d + 50;
        $d1 = 9 - substr($d, 0, 1);
        $d2 = 9 - substr($d, 1, 1);
        $d = $d1 . $d2;

        $firstpart = $m . $y . $d;
        return $firstpart;
    }

    public static function ENPSum($enp)
    {
        // нечетные цифры начниная справа
        $s1 = '';
        // четные цифры начиная справа
        $s2 = '';
        for ($i = 14; $i >= 0; $i--) {
            if ($i % 2 == 0)
                $s1 .= $enp[$i];
            else
                $s2 .= $enp[$i];
        }
        if ($s1 == '') {
            $s1 = 0;
        } else {
            $s1 = (int)$s1 * 2;
        }
        // приписываем "нечетное" число слева от "четного" умноженного на 2
        $s = (int)$s2 . (int)$s1;
        // складываем все цифры полученного числа
        $sum = 0;
        for ($i = 0; $i < strlen($s); $i++) {
            $u = (int)$s[$i];
            $sum += $u;
        }
        //  число вычитается из ближайшего большего или равного числа, кратного 10.
        $k = 10 - ($sum % 10);
        if ($k == 10)
            $k = 0;
        return $k;
    }

    public static function CheckENP($enp, DateTime $dateman, $sexman)
    {
        $count = strlen($enp);
        if ($count != 16) {
            return 1;
            //("Неверное количество символов в енп $enp - $count\n");
        }
        $k = self::ENPSum($enp);
        if ($k != $enp[15]) {
            return 2;
            //iconv_dos("Неверная контрольная сумма $k");
        }
        $firstpartReal = self::GetENPFirstpart($dateman, $sexman);
        $firstpartGet = substr($enp, 2, 8);
        if ($firstpartGet != $firstpartReal) {
            return 3;
            //iconv_dos("Неверна вычисляемая часть енп $enp $firstpartReal");
        }
        return true;
    }

    /**
     * @param $model PeopleBase
     * @return string
     * @throws \Exception
     */
    public function genenp($model)
    {

        $DateBirthday = new DateTime($model->dateMan);
        $c = self::CheckENP($model->ENP, $DateBirthday, $model->sexMan);

        $firstpart = self::GetENPFirstpart($DateBirthday, $model->sexMan);
        $maxserial = (new \yii\db\Query())
            ->select('MAX(KOL) maxserial')
            ->from('rez')
            ->where('DAT_R=:DAT_R', [':DAT_R' => $firstpart])
            ->scalar();

        if ($maxserial === null) {
            //С такой первой частью номера еще нету. То присваеваем номер и вставляем в базу
            $code = \Yii::$app->params['region'] . $firstpart . "00001";
            $k = self::ENPSum($code);
            $realcode = $code . $k;


            $insertrez = \yii::$app->db->createCommand()->insert('rez', ['dat_r' => $firstpart, 'kol' => 1]);
            if ($insertrez == 0) {
                throw new \Exception('Не удалось вставить строку в таблицу rez');
            }
        } else {
            //С такой первой частью номер есть. То присваеваем номер и вставляем в базу
            $maxserial++;
            $maxserial = str_pad($maxserial, 5, "0", STR_PAD_LEFT);
            $code = \yii::$app->params['region'] . $firstpart . $maxserial;
            $k = self::ENPSum($code);
            $realcode = $code . $k;
            $updaterez = \yii::$app->db->createCommand()->update('rez', ['KOL' => $maxserial], 'DAT_R = :DAT_R', [':DAT_R' => $firstpart])->execute();
            if ($updaterez == 0)
                throw new \Exception('Не удалось обновить таблицу rez');

        }
        if (!empty($realcode)) {
            $updatepeople = \yii::$app->db->createCommand()->update('people', ['enp' => $realcode], 'id = :id', [':id' => $model->id])->execute();
            if ($updatepeople == 0)
                throw new \Exception('Не удалось обновить ЕНП застрахованному с ID ' . $model->id);
        }
        return $realcode;
    }
}