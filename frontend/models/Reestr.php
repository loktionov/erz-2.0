<?php

namespace frontend\models;

use common\models\ReestrBase;
use yii;

/**
 * This is the model class for table "reestr".
 *
 * @property integer $id
 * @property string $ENumber
 * @property string $OldNumber
 * @property integer $ManId
 * @property integer $orgId
 * @property integer $typePoles
 * @property string $beginDate
 * @property string $endDate
 * @property string $stmDate
 * @property integer $stmType
 * @property integer $formPoles
 * @property integer $reasonStm
 * @property string $numBlank
 * @property string $numCard
 * @property integer $statusPoles
 * @property string $AnnulDate
 * @property string $InsDate
 * @property integer $cr
 * @property string $crdate
 * @property string $crcomment
 * @property integer $PVCode
 * @property integer $reasonG
 *
 * @property People $man
 * @property Smo $org
 * @property Typepoles $typePoles0
 */
class Reestr extends ReestrBase
{
    
}
