<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "req_pers".
 *
 * @property integer $id
 * @property integer $rid
 * @property string $sname
 * @property string $name
 * @property string $pname
 * @property string $dateman
 * @property integer $sexman
 * @property string $pbman
 * @property integer $prev
 */
class ReqPers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'req_pers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rid'], 'required'],
            [['rid', 'sexman', 'prev'], 'integer'],
            [['sname', 'name', 'pname', 'pbman'], 'string'],
            [['dateman'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rid' => 'RequestsID',
            'sname' => 'Фамилия',
            'name' => 'Имя',
            'pname' => 'Отчество',
            'dateman' => 'Дата рождения',
            'sexman' => 'Пол',
            'pbman' => 'Место рождения',
            'prev' => 'Prev',
        ];
    }
}
