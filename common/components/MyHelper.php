<?php
namespace common\components;

use yii\base\ErrorException;
use yii\base\Exception;

class MyHelper {
    static function getSexArray(){
        return [1=>'муж', 2=>'жен'];
    }
    static function getSexValue($sex){
        $i = array_key_exists((int)$sex, self::getSexArray());
        if($i === false)
            throw new Exception('Неверный индекс пола');
        return self::getSexArray()[(int)$sex];
    }
    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
     * @param  $number Integer Число на основе которого нужно сформировать окончание
     * @param $endingArray array Массив слов или окончаний для чисел (1, 4, 5), например array('яблоко', 'яблока', 'яблок')
     * @return String
     */
    public static function getNumEnding($number, $endingArray)
    {
        if (!is_numeric($number))
            return '';
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $endingArray[0];
                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $endingArray[1];
                    break;
                default:
                    $ending = $endingArray[2];
            }
        }
        return $ending;
    }

    public static function checkSNILS($snils, $allow_empty = true)
    {
        $empt = str_replace('0', '', $snils);
        if (empty($empt)) {
            if ($allow_empty)
                return true;
            else return false;
        }

        $snils = str_pad(str_replace('-', '', str_replace(' ', '', $snils)), 11, 0, STR_PAD_LEFT);
        $regex = '/^\d{11}$/';
        if (!preg_match($regex, $snils))
            return false;
        $regex = '/(\d)/';
        preg_match_all($regex, $snils, $matches);
        $matches = $matches[0];
        $end_check = (int)($matches[9] . $matches[10]);
        $minsnils = 1001998;
        $oursnils = '';
        $checksum = 0;
        $num = 0;
        for ($i = 0; $i < 9; $i++) {
            $num = (int)$matches[$i];
            $oursnils .= $matches[$i];
            $checksum += $num * (9 - $i);
        }
        //echo $oursnils.' - '.$checksum;
        if ($minsnils >= (int)$oursnils)
            return true;
        if ($checksum < 100) {
            if ($checksum != $end_check)
                return false;
            else return true;
        } elseif ($checksum == 100 OR $checksum == 101) {
            if ($end_check != 0)
                return false;
            else return true;
        } elseif ($checksum > 101) {
            $checksum = $checksum % 101;
            if ($end_check != (int)(substr((string)$checksum, -2)))
                return false;
            else return true;
        } else return false;
    }
}