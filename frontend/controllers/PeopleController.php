<?php

namespace frontend\controllers;

use common\models\Fiasaddrobj;
use frontend\assets\AppAsset;
use frontend\models\ApplyView;
use frontend\models\Reestr;
use kartik\mpdf\Pdf;
use yii;
use frontend\models\People;
use frontend\models\PeopleSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * PeopleController implements the CRUD actions for People model.
 */
class PeopleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => ['history'],
                'rules' => [
                    /*[
                        'allow' => true,
                        'actions' => ['history', 'getapply', 'index', 'index_admin', 'update','create','address_child'],
                        'roles' => ['@'],
                    ],*/
                    [
                        'allow' => true,
                        'actions' => ['history', 'getapply'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function actionGeturl()
    {
        return yii\bootstrap\Html::a("открыть", yii\helpers\Url::to(['people/getapply', 'reestrid' => 22025411]), ['target' => '_blank']);
    }

    public function actionGetapply($reestrid, $type='smo')
    {
        /** @var Pdf $pdf */
        $pdf = new Pdf([
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
        ]);


        if($type == 'tmp_polis'){
            $pdf->marginTop = 0;
            $pdf->marginBottom = 0;
            $pdf->marginLeft = 2;
            $pdf->marginRight = 0;
            $view = 'temp_polis';
        } else if($type == 'smo'){
            $view = 'apply_for_medpolis';
        } else if($type == 'duplicate'){
            $view = 'apply_for_medpolis_duplicate';
        } else throw new NotFoundHttpException();

        $apply_data = ApplyView::findOne($reestrid);
        $people = People::findOne($apply_data->peopleid);
        $addrs = $people->updateAddress();
//return $this->renderPartial($view, ['data' => $apply_data, 'reg' => $addrs['reg'], 'live' => $addrs['live']]);
        $pdf->content = $this->renderPartial($view, ['data' => $apply_data, 'reg' => $addrs['reg'], 'live' => $addrs['live']]);
        $pdf->cssFile = Yii::getAlias('@frontend/views/people/apply_print.css');

        $css = "{border-bottom: 1pt solid black;}";
        $me = empty($apply_data->pp) ? '.me ' : '.proxy ';
        $pdf->cssInline = $me . $css;




        return $pdf->render();

    }

    public function actionHistory($pid = null)
    {
        $people = $this->findModel($pid);
        echo $this->render('history', ['people' => $people]);
    }

    /**
     * Lists all People models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelSearch = new PeopleSearch();
        $query = $modelSearch->search(Yii::$app->request->queryParams);
        $c = $query->count('distinct people.id');
        $model = new People();
        if ($c == 1) {
            $model = $query->one();
        } else {
            \Yii::$app->getSession()->setFlash('error', "Количество результатов больше одного. Попробуйте сузить поиск.");
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'model' => $model,
        ]);
    }

    public function actionIndex_admin()
    {

        $model = new PeopleSearch();
        $dataProvider = $model->searchAdmin(Yii::$app->request->queryParams);

        return $this->render('index_admin', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single People model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new People model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new People();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing People model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        list ($regAddress, $liveAddress) = array_values($model->updateAddress());
        $proxy = $model->getProxy();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'regAddress' => $regAddress,
                'liveAddress' => $liveAddress,
                'proxy' => $proxy,
            ]);
        }
    }

    public function actionCreatereestr()
    {
        $model = new Reestr();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('reestr/create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdatereestr($id)
    {
        $model = $this->findModelReestr($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('reestr/update', [
                'model' => $model,
            ]);
        }
    }

    protected function findModelReestr($id)
    {
        if (($model = Reestr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddress_child()
    {

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $level = yii::$app->request->get('level');
                $cat_id = ($level == 'street' && isset($parents[1]) && preg_match('/\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/', $parents[1])) ? $parents[1] : $parents[0];
                $out = Fiasaddrobj::getChilds($cat_id, $level);

                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGenenp($id)
    {
        $model = $this->findModel($id);
        $model->genenp($model);
    }


    /**
     * Finds the People model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return People the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = People::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
