<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.07.2016
 * Time: 15:57
 */

namespace console\components;


use common\components\MyXmlReader;
use common\models\DispContent;
use common\models\DispFiles;
use common\models\DispFlk;
use common\models\DispZglv;

class DispXmlReader extends MyXmlReader
{
    /**
     * @var $file DispFiles
     */
    public $file;

    public function __construct(DispFiles $file)
    {
        $this->file = $file;
        // by node name
        $this->registerCallback("PERS", array($this, "callbackPERS"));
        // by xpath
        $this->registerCallback("/ZL_LIST/ZGLV", array($this, "callbackZGLV"));
    }

    /**
     * @param $reader self
     * @return bool
     */
    protected function callbackPERS($reader)
    {
        $xml = $reader->expandSimpleXml();
        $cntn = new DispContent();
        $cntn->fid = $this->file->id;
        $cntn->id_pac = $this->getSafeValue($xml, 'ID_PAC');
        $cntn->disp = $this->getSafeValue($xml, 'DISP');
        $cntn->pd = $this->getSafeValue($xml, 'PD');
        $cntn->fam = $this->getSafeValue($xml, 'FAM');
        $cntn->im = $this->getSafeValue($xml, 'IM');
        $cntn->ot = $this->getSafeValue($xml, 'OT');
        $cntn->w = $this->getSafeValue($xml, 'W');
        $cntn->dr = $this->getSafeValue($xml, 'DR');
        $cntn->doctype = $this->getSafeValue($xml, 'DOCTYPE');
        $cntn->docser = $this->getSafeValue($xml, 'DOCSER');
        $cntn->docnum = $this->getSafeValue($xml, 'DOCNUM');
        $cntn->snils = $this->getSafeValue($xml, 'SNILS');
        $cntn->vpolis = $this->getSafeValue($xml, 'VPOLIS');
        $cntn->spolis = $this->getSafeValue($xml, 'SPOLIS');
        $cntn->npolis = $this->getSafeValue($xml, 'NPOLIS');
        $cntn->code_mo = $this->getSafeValue($xml, 'CODE_MO');
        $cntn->lpu_pod = $this->getSafeValue($xml, 'LPU_POD');
        $cntn->coment = $this->getSafeValue($xml, 'COMENT');
        $cntn->action = $this->getSafeValue($xml, 'ACTION');
        $cntn->disp_os = $this->getSafeValue($xml, 'DISP_OS');
        if (!$cntn->save()) {
            $cntn->save(false);
            $cntn->saveFlk();
        }
        return true;
    }

    /**
     * @param $reader self
     * @return bool
     */
    protected function callbackZGLV($reader)
    {
        $xml = $reader->expandSimpleXml();
        $zglv = new DispZglv(['scenario' => DispZglv::SCENARIO_LINK]);
        $zglv->fid = $this->file->id;
        $zglv->vers = $this->getSafeValue($xml, 'VERSION');
        $zglv->fdate = $this->getSafeValue($xml, 'DATE');
        $zglv->filename = $this->getSafeValue($xml, 'FILENAME');
        $zglv->code_mo = $this->getSafeValue($xml, 'CODE_MO');
        $zglv->email = $this->getSafeValue($xml, 'EMAIL');
        $zglv->comment = $this->getSafeValue($xml, 'COMMENT');
        $valid = $zglv->validate();
        if (!$valid) {
            $this->file->errorsEx = array_merge($this->file->errorsEx, $zglv->errorsEx);
            //return false;
        }
        $this->file->link('zglv', $zglv);
        //return true;
        return $valid;
    }

    protected function getSafeValue($xml, $attr)
    {
        $attr = strtoupper($attr);
        if (!isset($xml->$attr))
            return null;
        return (string)($xml->$attr);
    }
}