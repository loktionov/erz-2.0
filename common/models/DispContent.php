<?php

namespace common\models;

use common\components\ConsoleActiveRecord;
use common\components\NameValidator;
use common\components\SnilsValidator;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "disp_content".
 *
 * @property integer $id
 * @property integer $fid
 * @property string $id_pac
 * @property string $disp
 * @property integer $pd
 * @property string $fam
 * @property string $im
 * @property string $ot
 * @property integer $w
 * @property string $dr
 * @property integer $doctype
 * @property string $docser
 * @property string $docnum
 * @property string $snils
 * @property integer $vpolis
 * @property string $spolis
 * @property string $npolis
 * @property string $code_mo
 * @property integer $lpu_pod
 * @property string $coment
 * @property integer $action
 * @property integer $disp_os
 */
class DispContent extends ConsoleActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pac', 'disp', 'fam', 'im', 'ot', 'docser', 'docnum', 'snils', 'spolis', 'npolis', 'code_mo', 'coment'], 'trim'],
            [['fid', 'pd', 'w', 'lpu_pod', 'id_pac', 'disp', 'fam', 'im', 'npolis', 'code_mo', 'dr'], 'required'],
            //[['vpolis', 'docnum',], 'required'],
            [['doctype',], 'required', 'when' => function ($model) {
                return false;
                //return !preg_match('/^\d{16}$/', $model->docnum);
            }],
            [['fid', 'pd', 'w', 'doctype', 'vpolis', 'lpu_pod'], 'integer'],
            [['id_pac', 'disp', 'fam', 'im', 'ot', 'docser', 'docnum', 'snils', 'spolis', 'npolis', 'code_mo', 'coment'], 'string'],
            [['dr'], 'safe'],
            [['fam', 'im', 'ot'], NameValidator::className()],
            //[['snils',], SnilsValidator::className()],
            [['disp',], 'V016'],
            [['pd',], 'match', 'pattern' => '/^[1-4]$/'],
            [['w',], 'match', 'pattern' => '/^[1-2]$/'],
            //[['vpolis',], 'match', 'pattern' => '/^[1-3]$/'],
            [['doctype',], 'F011'],
            [['code_mo',], 'F003'],
            [['lpu_pod',], 'F003PMO'],
            [['dr',], 'checkDr'],
            [['action',], 'action'],
            [['disp_os',], 'disp_os'],
            //[['doctype',], 'Documents'],
        ];
    }

    public function checkDr()
    {
        $age = 2000 + $this->getFile()->one()->y - date('Y', strtotime($this->dr));
        if ($age < 18) {
            $this->addError('dr', 'Неверная возрастная группа. Допускаются данные только о взрослом населении.');
        } else if (($age % 3) AND empty($this->disp_os)){
            $this->addError('dr', 'Неверная возрастная группа.');
        }
    }

    public function Documents()
    {
        if (empty($this->doctype) and preg_match('/^\d{16}$/', $this->docnum))
            return;
        $q['1'] = $q['3'] = '/^[1IVXLC]{1,6}-[А-Я]{2}&\d{6}$/iu';
        $q['2'] =
        $q['5'] =
        $q['9'] =
        $q['10'] =
        $q['11'] =
        $q['12'] =
        $q['13'] =
        $q['22'] =
        $q['23'] =
        $q['24'] =
        $q['27'] =
        $q['28'] =
            '/^[А-Я\d\-\s]{0,12}&\d{1,12}$|^([A-Z\d\-\s]{0,12}&\d{1,12})$/iu';

        $q['23'] = '/^[А-ЯA-Z\d\-\s]{0,12}&\d{1,12}$/iu';

        $q['4'] = '/^[А-Я]{2}&\d{7}$/iu';
        $q['6'] = $q['17'] = '/^[А-Я]{2}&\d{6}$/iu';
        $q['7'] = $q['16'] = '/^[А-Я]{2}&\d{6,7}$/iu';
        $q['8'] = $q['15'] = '/^\d\d&\d{7}$/iu';
        $q['14'] = '/^\d\d\s\d\d&\d{6}$/iu';
        $q['18'] = '/^[\d\sA-ZА-Я\-]{0,12}&\d{1,12}$/iu';
        $q['21'] = '/^[А-ЯA-Z\d\-\s]{0,12}&\d{1,12}$/iu';
        $q['25'] = '/^([А-Я]{2}|\d{2})&\d{7}$/iu';
        $q['26'] = '/^&\d{6}/iu';

        if (!preg_match($q[$this->doctype], $this->docser . '&' . $this->docnum)) {
            $this->addError('doctype', 'Данные документа не соответствуют маске');
        }
    }

    public function disp_os($attr)
    {
        if (empty(DispOs::findOne(['id' => $this->$attr])))
            $this->addError($attr, "Неверный код особых случаев диспанцсеризации");
    }

    public function action($attr)
    {
        if (empty(DispActions::findOne(['id' => $this->$attr])))
            $this->addError($attr, "Неверный код типа действия");
    }

    public function V016($attr)
    {
        if (empty(V016::findOne(['code' => $this->$attr, 'disp_type' => [12, 22]])))
            $this->addError($attr, "Неверный код типа диспансеризации. Допускаются данные только о взрослом населении, коды ДВ1 и ОПВ.");
    }

    public function F003($attr)
    {
        if (empty(F003::findOne(['MCOD' => $this->$attr])))
            $this->addError($attr, "Код «{$this->getAttributeLabel($attr)}» не соответствует справочнику F003");
    }

    public function F003PMO($attr)
    {
        if (empty(F003PMO::findOne(['kod' => $this->$attr])))
            $this->addError($attr, "Код «{$this->getAttributeLabel($attr)}» не соответствует справочнику F003");
    }

    public function F011($attr)
    {
        if (empty(Typedoc::findOne(['id' => $this->$attr])))
            $this->addError($attr, "Код «{$this->getAttributeLabel($attr)}» не соответствует справочнику F011");
    }

    /**
     * @return DispFiles
     */
    public function getFile()
    {
        return $this->hasOne(DispFiles::className(), ['id' => 'fid']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fid' => 'Fid',
            'id_pac' => 'Код записи о пациенте',
            'disp' => 'Тип диспанцсеризации',
            'pd' => 'Период диспансеризации',
            'fam' => 'Фамилия',
            'im' => 'Имя',
            'ot' => 'Отчество',
            'w' => 'Пол',
            'dr' => 'Дата рождения',
            'doctype' => 'Тип документа',
            'docser' => 'Серия документа',
            'docnum' => 'Номер документа',
            'snils' => 'СНИЛС',
            'vpolis' => 'Вид полиса',
            'spolis' => 'Серия полиса',
            'npolis' => 'Номер полиса',
            'code_mo' => 'Код МО',
            'lpu_pod' => 'Код ПМО',
            'coment' => 'Коментарий',
        ];
    }

    public function saveFlk($errors = [])
    {
        if (empty($errors))
            $errors = $this->errorsEx;
        foreach ($errors as $a => $err) {
            $flk = new DispFlk(['scenario' => self::SCENARIO_LINK]);
            $flk->attr = $a;
            $flk->strict = DispFlk::STRICT_ERROR;
            $flk->ecode = key($err);
            $flk->emess = array_shift($err);
            $flk->id_pac = $this->id_pac;
            $flk->fid = $this->fid;
            $flk->save(false);
        }
    }

    public function identification()
    {
        $enp = preg_match('/^\d{16}$/', $this->docnum) ? $this->docnum : '';
        $params = [
            ':fam' => $this->fam,
            ':im' => $this->im,
            ':ot' => $this->ot,
            ':dr' => date('d-m-Y', strtotime($this->dr)),
            ':enp' => $enp,
            ':npolis' => $this->npolis,
            ':docser' => $this->docser,
            ':docnum' => $this->docnum,
            ':snils' => $this->snils,
            ':edate' => date('d-m-Y', strtotime('01.' . ($this->pd * 3) . '.' . $this->file->y)),
        ];
        $res = Yii::$app->db
            ->createCommand("SELECT * FROM FnMyInfoPolisPlus(:fam,:im,:ot,:dr,:enp,:npolis,:docser,:docnum,:snils,:edate)", $params)
            ->queryOne();
        if ($res) {
            $ident = new DispIdent();
            $ident->pid = $res['id'];
            $ident->smo = $res['smo'];
            $this->link('ident', $ident);
        } else {
            $this->saveFlk(['id_pac' => ['not_ident' => 'Не удалось определить страховую принадлежность']]);
        }
    }

    public function getIdent()
    {
        return $this->hasOne(DispIdent::className(), ['cid' => 'id']);
    }
}
