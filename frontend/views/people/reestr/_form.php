<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reestr */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reestr-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'ManId')->hiddenInput()->label(false); ?>



    <?= $form->field($model, 'typePoles')->dropDownList(

        \common\models\Typepolis::find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column(),
        ['prompt' => 'Выберите тип полиса']

    ); ?>

    <?= $form->field($model, 'ENumber')->textInput() ?>

    <?= $form->field($model, 'beginDate')->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
        ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999']) ?>

    <?= $form->field($model, 'endDate')->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
        ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999']) ?>

    <?= $form->field($model, 'stmDate')->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
        ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999']) ?>

    <?= $form->field($model, 'stmType')->textInput() ?>

    <?= $form->field($model, 'formPoles')->textInput() ?>

    <?= $form->field($model, 'reasonStm')->textInput() ?>

    <?= $form->field($model, 'numBlank')->textInput() ?>

    <?= $form->field($model, 'numCard')->textInput() ?>

    <?= $form->field($model, 'statusPoles')->textInput() ?>

    <?= $form->field($model, 'AnnulDate')->textInput() ?>

    <?= $form->field($model, 'InsDate')->textInput() ?>

    <?= $form->field($model, 'PVCode')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
