<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\People */
/* @var $form ActiveForm */


$form = ActiveForm::begin(); ?>
<ul id="w0" class="nav nav-tabs">
    <li class="active"><a href="#fio" data-toggle="tab" aria-expanded="true">ФИО</a></li>
    <li class=""><a href="#docs" data-toggle="tab" aria-expanded="false">Документы</a></li>
    <li class=""><a href="#address" data-toggle="tab" aria-expanded="false">Адрес</a></li>
    <li class=""><a href="#proxy" data-toggle="tab" aria-expanded="false">Представитель</a></li>
    <li class=""><a href="#service" data-toggle="tab" aria-expanded="false">Служебная инфо</a></li>
</ul>
<div class="tab-content">
    <div id="fio" class="tab-pane active">
        <?= $form->field($model, 'ENP') ?>
        <?= $form->field($model, 'Name') ?>
        <?= $form->field($model, 'sName') ?>
        <?= $form->field($model, 'pName') ?>
        <?= $form->field($model, 'pbMan') ?>
        <?= $form->field($model, 'dateMan') ?>
        <?= $form->field($model, 'sexMan') ?>
        <?= $form->field($model, 'nationMan') ?>
        <?= $form->field($model, 'contact') ?>
    </div>
    <div id="docs" class="tab-pane">
        <?= $form->field($model, 'serDoc') ?>
        <?= $form->field($model, 'numDoc') ?>
        <?= $form->field($model, 'snils') ?>
        <?= $form->field($model, 'orgdoc') ?>
        <?= $form->field($model, 'dateDoc') ?>
        <?= $form->field($model, 'doc_date_end') ?>
        <?= $form->field($model, 'typeDoc') ?>
    </div>
    <div id="address" class="tab-pane">
        <?= $form->field($model, 'addressReg') ?>
        <?= $form->field($model, 'addressLive') ?>
        <?= $form->field($model, 'okato_mj') ?>
        <?= $form->field($model, 'okato_reg') ?>
        <?= $form->field($model, 'kladr_reg') ?>
        <?= $form->field($model, 'kladr_live') ?>
        <?= $form->field($model, 'dateReg') ?>
        <?= $form->field($model, 'regKladrId') ?>
        <?= $form->field($model, 'liveKladrId') ?>
    </div>

    <div id="proxy" class="tab-pane">
        <?= $form->field($model, 'proxy_man') ?>
    </div>

    <div id="service" class="tab-pane">
        <?= $form->field($model, 'dateDeath') ?>
        <?= $form->field($model, 'InsDate') ?>
        <?= $form->field($model, 'crdate') ?>
        <?= $form->field($model, 'statusMan') ?>
        <?= $form->field($model, 'activPolesId') ?>
        <?= $form->field($model, 'cr') ?>
        <?= $form->field($model, 'lostman') ?>
        <?= $form->field($model, 'livetemp') ?>
        <?= $form->field($model, 'refugee') ?>
        <?= $form->field($model, 'FFOMS') ?>
        <?= $form->field($model, 'wid') ?>
        <?= $form->field($model, 'pr_rab') ?>
    </div>


</div>
<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

