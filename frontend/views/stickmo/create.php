<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StickMO */

$this->title = 'Прикрепить ' . $fio;
$this->params['breadcrumbs'][] = ['label' => 'Stick Mos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stick-mo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
