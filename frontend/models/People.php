<?php

namespace frontend\models;

use common\components\EnpBehavior;
use yii;
use yii\data\ArrayDataProvider;
use common\models\PeopleBase;

/**
 * This is the model class for table "people".
 */
class People extends PeopleBase
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'genenp' => EnpBehavior::className(),

        ]);
    }

    public function GetHistoryDp()
    {
        $data = Yii::$app->db
            ->createCommand('select distinct * from fnGetFullHistory(:pid) order by editdate', [':pid' => $this->id])
            ->queryAll();

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
            'sort' => false,
        ]);
    }
}
