<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 05.08.2016
 * Time: 11:46
 */

namespace frontend\controllers;


use common\models\DispFiles;
use common\models\DispFilesSearch;
use common\models\DispFlk;
use common\models\DispFlkSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;


class DispController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['history'],
                'rules' => [
                    [
                        'allow' => false,
                        //'denyCallback' => function($rule, $action){Yii::$app->end();},
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new DispFilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionFlk($fid)
    {
        /*if(!Yii::$app->request->isAjax)
            return;*/


        $searchModel = new DispFlkSearch();
        $dataProvider = $searchModel->search(['DispFlkSearch'=>['fid' => $fid]]);

        return $this->renderPartial('_ajax', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


}