<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "V016".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $bdate
 * @property string $edate
 * @property integer $disp_type
 */
class V016 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'V016';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'string'],
            [['bdate', 'edate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'bdate' => 'Bdate',
            'edate' => 'Edate',
            'disp_type' => 'disp_type',
        ];
    }
}
