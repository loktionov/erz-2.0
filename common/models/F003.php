<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "F003".
 *
 * @property string $MCOD
 * @property string $TF_OKATO
 * @property string $NAM_MOP
 * @property string $NAM_MOK
 * @property string $NAME
 * @property string $INN
 * @property string $OGRN
 * @property string $KPP
 * @property string $INDEX_J
 * @property string $ADDR_J
 * @property string $OKOPF
 * @property string $VEDPRI
 * @property string $FAM_RUK
 * @property string $IM_RUK
 * @property string $OT_RUK
 * @property string $PHONE
 * @property string $FAX
 * @property string $E_MAIL
 * @property string $SOLIST
 * @property string $WWW
 * @property string $D_BEGIN
 * @property string $DUVED
 * @property string $D_EDIT
 * @property string $D_END
 * @property string $NAME_E
 * @property string $DOC
 * @property string $PODR
 */
class F003 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'F003';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MCOD', 'TF_OKATO', 'NAM_MOP', 'NAM_MOK', 'NAME', 'INN', 'OGRN', 'KPP', 'INDEX_J', 'ADDR_J', 'OKOPF', 'VEDPRI', 'FAM_RUK', 'IM_RUK', 'OT_RUK', 'PHONE', 'FAX', 'E_MAIL', 'SOLIST', 'WWW', 'D_BEGIN', 'DUVED', 'D_EDIT', 'D_END', 'NAME_E', 'DOC', 'PODR'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'MCOD' => 'Mcod',
            'TF_OKATO' => 'Tf  Okato',
            'NAM_MOP' => 'Nam  Mop',
            'NAM_MOK' => 'Nam  Mok',
            'NAME' => 'Name',
            'INN' => 'Inn',
            'OGRN' => 'Ogrn',
            'KPP' => 'Kpp',
            'INDEX_J' => 'Index  J',
            'ADDR_J' => 'Addr  J',
            'OKOPF' => 'Okopf',
            'VEDPRI' => 'Vedpri',
            'FAM_RUK' => 'Fam  Ruk',
            'IM_RUK' => 'Im  Ruk',
            'OT_RUK' => 'Ot  Ruk',
            'PHONE' => 'Phone',
            'FAX' => 'Fax',
            'E_MAIL' => 'E  Mail',
            'SOLIST' => 'Solist',
            'WWW' => 'Www',
            'D_BEGIN' => 'D  Begin',
            'DUVED' => 'Duved',
            'D_EDIT' => 'D  Edit',
            'D_END' => 'D  End',
            'NAME_E' => 'Name  E',
            'DOC' => 'Doc',
            'PODR' => 'Podr',
        ];
    }
}
