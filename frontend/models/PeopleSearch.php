<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * PeopleSearch represents the model behind the search form about `frontend\models\People`.
 */
class PeopleSearch extends People
{

    public $polis;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sexMan', 'typeDoc', 'statusMan', 'regKladrId', 'activPolesId', 'liveKladrId', 'cr', 'lostman', 'livetemp', 'refugee', 'FFOMS', 'wid'], 'integer'],
            [['ENP', 'Name', 'sName', 'pName', 'dateMan', 'pbMan', 'nationMan', 'serDoc', 'numDoc', 'dateDoc', 'addressReg',
                'dateReg', 'addressLive', 'dateDeath', 'snils', 'okato_mj', 'InsDate', 'crdate', 'proxy_man', 'contact',
                'doc_date_end', 'orgdoc', 'okato_reg', 'kladr_reg', 'kladr_live', 'polis'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['polis' => 'Полис']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAdmin($params)
    {
        $query = People::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'dateMan' => $this->dateMan,
            'sexMan' => $this->sexMan,
            'typeDoc' => $this->typeDoc,
            'dateDoc' => $this->dateDoc,
            'statusMan' => $this->statusMan,
            'regKladrId' => $this->regKladrId,
            'dateReg' => $this->dateReg,
            'activPolesId' => $this->activPolesId,
            'dateDeath' => $this->dateDeath,
            'liveKladrId' => $this->liveKladrId,
            'InsDate' => $this->InsDate,
            'cr' => $this->cr,
            'crdate' => $this->crdate,
            'lostman' => $this->lostman,
            'doc_date_end' => $this->doc_date_end,
            'livetemp' => $this->livetemp,
            'refugee' => $this->refugee,
            'FFOMS' => $this->FFOMS,
            'wid' => $this->wid,
        ]);

        $query->andFilterWhere(['like', 'ENP', $this->ENP])
            ->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'sName', $this->sName])
            ->andFilterWhere(['like', 'pName', $this->pName])
            ->andFilterWhere(['like', 'pbMan', $this->pbMan])
            ->andFilterWhere(['like', 'nationMan', $this->nationMan])
            ->andFilterWhere(['like', 'serDoc', $this->serDoc])
            ->andFilterWhere(['like', 'numDoc', $this->numDoc])
            ->andFilterWhere(['like', 'addressReg', $this->addressReg])
            ->andFilterWhere(['like', 'addressLive', $this->addressLive])
            ->andFilterWhere(['like', 'snils', $this->snils])
            ->andFilterWhere(['like', 'okato_mj', $this->okato_mj])
            ->andFilterWhere(['like', 'proxy_man', $this->proxy_man])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'orgdoc', $this->orgdoc])
            ->andFilterWhere(['like', 'okato_reg', $this->okato_reg])
            ->andFilterWhere(['like', 'kladr_reg', $this->kladr_reg])
            ->andFilterWhere(['like', 'kladr_live', $this->kladr_live]);

        $query->joinWith(['reestrs' => function($query){
            $query->andFilterWhere(['=', 'reestr.enumber', $this->polis]);
        }]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveQuery
     */
    public function search($params)
    {
        $query = People::find();

        if (!($this->load($params) && $this->validate())) {
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'dateMan' => $this->dateMan,
            'sexMan' => $this->sexMan,
            'typeDoc' => $this->typeDoc,
            'dateDoc' => $this->dateDoc,
            'statusMan' => $this->statusMan,
            'regKladrId' => $this->regKladrId,
            'dateReg' => $this->dateReg,
            'activPolesId' => $this->activPolesId,
            'dateDeath' => $this->dateDeath,
            'liveKladrId' => $this->liveKladrId,
            'InsDate' => $this->InsDate,
            'cr' => $this->cr,
            'crdate' => $this->crdate,
            'lostman' => $this->lostman,
            'doc_date_end' => $this->doc_date_end,
            'livetemp' => $this->livetemp,
            'refugee' => $this->refugee,
            'FFOMS' => $this->FFOMS,
            'wid' => $this->wid,
        ]);

        $query->andFilterWhere(['=', 'ENP', $this->ENP])
            ->andFilterWhere(['=', 'Name', $this->Name])
            ->andFilterWhere(['=', 'sName', $this->sName])
            ->andFilterWhere(['=', 'pName', $this->pName])
            ->andFilterWhere(['like', 'pbMan', $this->pbMan])
            ->andFilterWhere(['like', 'nationMan', $this->nationMan])
            ->andFilterWhere(['=', 'serDoc', $this->serDoc])
            ->andFilterWhere(['=', 'numDoc', $this->numDoc])
            ->andFilterWhere(['like', 'addressReg', $this->addressReg])
            ->andFilterWhere(['like', 'addressLive', $this->addressLive])
            ->andFilterWhere(['=', 'snils', $this->snils])
            ->andFilterWhere(['like', 'okato_mj', $this->okato_mj])
            ->andFilterWhere(['like', 'proxy_man', $this->proxy_man])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'orgdoc', $this->orgdoc])
            ->andFilterWhere(['like', 'okato_reg', $this->okato_reg])
            ->andFilterWhere(['like', 'kladr_reg', $this->kladr_reg])
            ->andFilterWhere(['like', 'kladr_live', $this->kladr_live]);

        $query->joinWith(['reestrs' => function($query){
            $query->andFilterWhere(['=', 'reestr.enumber', $this->polis]);
        }]);

        return $query;
    }
}
