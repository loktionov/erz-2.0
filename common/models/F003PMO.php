<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "F003_PMO".
 *
 * @property string $kod
 * @property double $ВладелецКод
 * @property string $ПометкаУдаления
 * @property string $Наименование
 * @property string $ADDR_P
 * @property string $TYPE_BRANCH
 * @property string $УровеньОМПАмбулаторнойПомощи
 * @property string $УровеньОМПСтационар
 * @property string $УровеньОМПДневнойСтационар
 * @property string $УровеньОМПСтоматология
 * @property string $УровеньОМПСкораяПомощь
 * @property string $Предопределенный
 * @property string $ИмяПредопределенныхДанных
 */
class F003PMO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'F003_PMO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod', 'ПометкаУдаления', 'Наименование', 'ADDR_P', 'TYPE_BRANCH', 'УровеньОМПАмбулаторнойПомощи', 'УровеньОМПСтационар', 'УровеньОМПДневнойСтационар', 'УровеньОМПСтоматология', 'УровеньОМПСкораяПомощь', 'Предопределенный', 'ИмяПредопределенныхДанных'], 'string'],
            [['ВладелецКод'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod' => 'Kod',
            'ВладелецКод' => 'ВладелецКод',
            'ПометкаУдаления' => 'ПометкаУдаления',
            'Наименование' => 'Наименование',
            'ADDR_P' => 'Addr  P',
            'TYPE_BRANCH' => 'Type  Branch',
            'УровеньОМПАмбулаторнойПомощи' => 'УровеньОМПАмбулаторнойПомощи',
            'УровеньОМПСтационар' => 'УровеньОМПСтационар',
            'УровеньОМПДневнойСтационар' => 'УровеньОМПДневнойСтационар',
            'УровеньОМПСтоматология' => 'УровеньОМПСтоматология',
            'УровеньОМПСкораяПомощь' => 'УровеньОМПСкораяПомощь',
            'Предопределенный' => 'Предопределенный',
            'ИмяПредопределенныхДанных' => 'ИмяПредопределенныхДанных',
        ];
    }
}
