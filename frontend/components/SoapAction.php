<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 20.02.2017
 * Time: 10:59
 */

namespace frontend\components;


use mongosoft\soapserver\Action;
use Yii;

class SoapAction extends Action
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->provider === null) {
            $this->provider = $this->controller;
        }
        if (isset($_GET[$this->serviceVar]))
            $hostInfo = 'http://192.168.1.220/erz2/frontend/web/index.php/api/soap';
        else
            $hostInfo = 'http://soap.tfomssk.ru/erz2/frontend/web/index.php/api/soap';
        if ($this->serviceUrl === null) {
            $this->serviceUrl = $hostInfo . '?ws=1';
            $this->serviceUrl = Yii::$app->getUrlManager()->createAbsoluteUrl([$this->getUniqueId(), $this->serviceVar => 1]);
        }
        if ($this->wsdlUrl === null) {
            $this->wsdlUrl = $hostInfo;
            $this->wsdlUrl = Yii::$app->getUrlManager()->createAbsoluteUrl([$this->getUniqueId()]);
        }

        // Disable CSRF (Cross-Site Request Forgery) validation for this action.
        Yii::$app->getRequest()->enableCsrfValidation = false;
    }
}