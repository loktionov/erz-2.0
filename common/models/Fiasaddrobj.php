<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ADDROBJ".
 *
 * @property integer $ACTSTATUS
 * @property string $AOGUID
 * @property string $AOID
 * @property integer $AOLEVEL
 * @property string $AREACODE
 * @property string $AUTOCODE
 * @property integer $CENTSTATUS
 * @property string $CITYCODE
 * @property string $CODE
 * @property integer $CURRSTATUS
 * @property string $ENDDATE
 * @property string $FORMALNAME
 * @property string $IFNSFL
 * @property string $IFNSUL
 * @property string $NEXTID
 * @property string $OFFNAME
 * @property string $OKATO
 * @property string $OKTMO
 * @property integer $OPERSTATUS
 * @property string $PARENTGUID
 * @property string $PLACECODE
 * @property string $PLAINCODE
 * @property string $POSTALCODE
 * @property string $PREVID
 * @property string $REGIONCODE
 * @property string $SHORTNAME
 * @property string $STARTDATE
 * @property string $STREETCODE
 * @property string $TERRIFNSFL
 * @property string $TERRIFNSUL
 * @property string $UPDATEDATE
 * @property string $CTARCODE
 * @property string $EXTRCODE
 * @property string $SEXTCODE
 * @property integer $LIVESTATUS
 * @property string $NORMDOC
 */
class Fiasaddrobj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ADDROBJ';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_fias');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ACTSTATUS', 'AOLEVEL', 'CENTSTATUS', 'CURRSTATUS', 'OPERSTATUS', 'LIVESTATUS'], 'integer'],
            [['AOGUID', 'AOID', 'AREACODE', 'AUTOCODE', 'CITYCODE', 'CODE', 'FORMALNAME', 'IFNSFL', 'IFNSUL', 'NEXTID', 'OFFNAME', 'OKATO', 'OKTMO', 'PARENTGUID', 'PLACECODE', 'PLAINCODE', 'POSTALCODE', 'PREVID', 'REGIONCODE', 'SHORTNAME', 'STREETCODE', 'TERRIFNSFL', 'TERRIFNSUL', 'CTARCODE', 'EXTRCODE', 'SEXTCODE', 'NORMDOC'], 'string'],
            [['ENDDATE', 'STARTDATE', 'UPDATEDATE'], 'safe'],
        ];
    }

    public static function getRegions()
    {
        return self::find()->where("LIVESTATUS=:actual AND PARENTGUID is null", [':actual' => 1])->select(["concat(FORMALNAME,' ',SHORTNAME)", 'AOGUID'])->indexBy('AOGUID')->orderBy('FORMALNAME')->column();
        //return self::find()->where("ACTSTATUS=:actual AND PARENTGUID is null", [':actual' => 1])->all();
    }

    public static function getObjsByParent($parent, $lvl)
    {
        if(empty($parent))
            return null;
        return self::find()->where(['LIVESTATUS' => 1, 'PARENTGUID' => $parent, 'AOLEVEL' => PeopleAddress::getLvlArray($lvl)])->select(["concat(SHORTNAME, '. ', FORMALNAME)", 'AOGUID'])->indexBy('AOGUID')->orderBy('FORMALNAME')->column();
        //return self::find()->where("ACTSTATUS=:actual AND PARENTGUID is null", [':actual' => 1])->all();
    }

    public static function getChilds($guid, $level = null)
    {
        $query = self::find()->where("LIVESTATUS=:actual AND PARENTGUID = :guid", [':actual' => 1, ':guid' => $guid])
            ->select(["concat(SHORTNAME, '. ', FORMALNAME) as name", 'AOGUID as id'])
            ->orderBy('FORMALNAME')->asArray();

        switch ($level){
            case 'city':
                $query->andWhere("AOLEVEL in (4,5,6)");
                break;
            case 'street':
                $query->andWhere("AOLEVEL > 6");
                break;
            default:
                break;
        }
        return $query->all();
    }

    public function attributeLabels()
    {
        return [
            'AOID' => 'Уникальный идентификатор записи',
            'FORMALNAME' => 'Формализованное наименование',
            'REGIONCODE' => 'Код региона',
            'AUTOCODE' => 'Код автономии',
            'AREACODE' => 'Код района',
            'CITYCODE' => 'Код города',
            'CTARCODE' => 'Код внутригородского района',
            'PLACECODE' => 'Код населенного пункта',
            'STREETCODE' => 'Код улицы',
            'EXTRCODE' => 'Код дополнительного адресообразующего элемента',
            'SEXTCODE' => 'Код подчиненного дополнительного адресообразующего элемента',
            'OFFNAME' => 'Официальное наименование',
            'POSTALCODE' => 'Почтовый индекс',
            'IFNSFL' => 'Код ИФНС ФЛ',
            'TERRIFNSFL' => 'Код территориального участка ИФНС ФЛ',
            'IFNSUL' => 'Код ИФНС ЮЛ',
            'TERRIFNSUL' => 'Код территориального участка ИФНС ЮЛ',
            'OKATO' => 'ОКАТО',
            'OKTMO' => 'ОКТМО',
            'UPDATEDATE' => 'Дата  внесения записи',
            'SHORTNAME' => 'Краткое наименование типа объекта',
            'AOLEVEL' => 'Уровень адресного объекта ',
            'PARENTGUID' => 'Идентификатор объекта родительского объекта',
            'AOGUID' => 'Глобальный уникальный идентификатор адресного объекта',
            'PREVID' => 'Идентификатор записи связывания с предыдушей исторической записью',
            'NEXTID' => 'Идентификатор записи  связывания с последующей исторической записью',
            'CODE' => 'Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0',
            'PLAINCODE' => 'Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)',
            'ACTSTATUS' => 'Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте.',
            'CENTSTATUS' => 'Статус центра',
            'OPERSTATUS' => 'Статус действия над записью – причина появления записи',
            'CURRSTATUS' => 'Статус актуальности КЛАДР 4 (последние две цифры в коде)',
            'STARTDATE' => 'Начало действия записи',
            'ENDDATE' => 'Окончание действия записи',
            'NORMDOC' => 'Внешний ключ на нормативный документ',
            'LIVESTATUS' => 'Livestatus',
        ];
    }
}
