<?php

namespace common\models;

use common\components\ConsoleActiveRecord;
use Yii;

/**
 * This is the model class for table "disp_flk".
 *
 * @property integer $id
 * @property integer $fid
 * @property string $strict
 * @property string $attr
 * @property string $ecode
 * @property string $emess
 * @property string $id_pac
 */
class DispFlk extends ConsoleActiveRecord
{
    const STRICT_ERROR = 'error';
    const STRICT_WARNING = 'warning';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_flk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid'], 'required'],
            [['fid'], 'integer'],
            [['strict', 'ecode', 'emess', 'id_pac'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LINK] = ['strict', 'attr', 'ecode', 'emess', 'id_pac'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fid' => 'Fid',
            'strict' => 'Strict',
            'attr' => 'Атрибут',
            'ecode' => 'Ecode',
            'emess' => 'Emess',
            'id_pac' => 'id_pac',
        ];
    }
}
