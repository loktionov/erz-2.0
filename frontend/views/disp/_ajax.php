<?php
/**
 * @var $dataProvider \common\models\DispFiles
 * @var $searchModel \common\models\DispFilesSearch
 * @var $this \yii\web\View
 *
 *
 */
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
]);