<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Reestr;

/**
 * ReestrSearch represents the model behind the search form about `frontend\models\Reestr`.
 */
class ReestrSearch extends Reestr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ManId', 'orgId', 'typePoles', 'stmType', 'formPoles', 'reasonStm', 'statusPoles', 'cr', 'PVCode', 'reasonG'], 'integer'],
            [['ENumber', 'OldNumber', 'beginDate', 'endDate', 'stmDate', 'numBlank', 'numCard', 'AnnulDate', 'InsDate', 'crdate', 'crcomment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reestr::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ManId' => $this->ManId,
            'orgId' => $this->orgId,
            'typePoles' => $this->typePoles,
            'beginDate' => $this->beginDate,
            'endDate' => $this->endDate,
            'stmDate' => $this->stmDate,
            'stmType' => $this->stmType,
            'formPoles' => $this->formPoles,
            'reasonStm' => $this->reasonStm,
            'statusPoles' => $this->statusPoles,
            'AnnulDate' => $this->AnnulDate,
            'InsDate' => $this->InsDate,
            'cr' => $this->cr,
            'crdate' => $this->crdate,
            'PVCode' => $this->PVCode,
            'reasonG' => $this->reasonG,
        ]);

        $query->andFilterWhere(['like', 'ENumber', $this->ENumber])
            ->andFilterWhere(['like', 'OldNumber', $this->OldNumber])
            ->andFilterWhere(['like', 'numBlank', $this->numBlank])
            ->andFilterWhere(['like', 'numCard', $this->numCard])
            ->andFilterWhere(['like', 'crcomment', $this->crcomment]);

        return $dataProvider;
    }
}
