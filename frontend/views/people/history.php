<?php
/**
 * @var $people \frontend\models\People
 */
use yii\grid\GridView;
use yii\helpers\Html;
use frontend\models\Typedocument;
echo GridView::widget([
    'dataProvider' => $people->GetHistoryDp(),
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => '№',
        ],
        [
            'attribute' => 'enp',
            'header' => 'ЕНП',
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'sname',
            'header' => 'Фамилия',
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'name',
            'header' => 'Имя',
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'pname',
            'header' => 'Отчество',
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'dateman',
            'header' => 'Дата рождения',
            'format' => ['date', 'php:d.m.Y'],
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'sexman',
            'header' => 'Пол',
            'value' => function($data){
                    return \common\components\MyHelper::getSexValue($data["sexman"]);
                },
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'typedoc',
            'header' => 'Документ',
            'format' => 'raw',
            'value' => function($data){
                    $docname = Typedocument::find()->andWhere('id=:id', [':id' => $data['typedoc']])->one();

                    return Html::tag('span', $data['typedoc'], [
                        'title'=>isset($docname) ? $docname->Name : 'null',
                        'data-toggle'=>'tooltip',
                        'style'=>'text-decoration: none; border-bottom: 1px dashed; cursor:pointer;'
                    ]) . ' ' . $data['serdoc'] . ' № ' . $data['numdoc'];
                },
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return array_merge(getContentOptions($data, $index, $col),
                        getContentOptions($data, $index, $col, 'serdoc'),
                        getContentOptions($data, $index, $col, 'numdoc'));

                }

        ],
        [
            'attribute' => 'pbman',
            'header' => 'Место рождения',
            'contentOptions' =>
                function ($data, $key, $index, $col) {

                    return getContentOptions($data, $index, $col);

                }

        ],
        [
            'attribute' => 'editdate',
            'header' => 'Дата изменения',
            'format' => ['date', 'php:d.m.Y'],
        ],
    ]
]);

/**
 * @param $data
 * @param $index
 * @param $col \yii\grid\DataColumn
 * @param null $attr
 * @return array
 */
function getContentOptions($data, $index, $col, $attr = null)
{
    $attr = isset($attr) ? $attr : $col->attribute;
    if ($index == 0)
        return [];
    $d = $col->grid->dataProvider->allModels;
    if ($data[$attr] != $d[$index - 1][$attr])
        return ['style' => 'background-color: pink;'];
    return [];
}