<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "O002".
 *
 * @property integer $IDKOD
 * @property string $KOD
 * @property string $NAME11
 * @property string $NAME12
 * @property string $ALFA2
 * @property string $ALFA3
 * @property string $DATEBEG
 * @property string $DATEEND
 */
class Nation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'O002';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KOD', 'NAME11', 'NAME12', 'ALFA2', 'ALFA3', 'DATEBEG', 'DATEEND'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IDKOD' => 'Idkod',
            'KOD' => 'Kod',
            'NAME11' => 'Name11',
            'NAME12' => 'Name12',
            'ALFA2' => 'Alfa2',
            'ALFA3' => 'Alfa3',
            'DATEBEG' => 'Datebeg',
            'DATEEND' => 'Dateend',
        ];
    }
}
