<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "spFonds".
 *
 * @property integer $id
 * @property string $Code
 * @property string $Name
 * @property string $OKATO
 */
class OKATO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spFonds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code', 'Name', 'OKATO'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Code' => 'Code',
            'Name' => 'Name',
            'OKATO' => 'Okato',
        ];
    }
}
