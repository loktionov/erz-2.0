<?php

namespace common\models;
use frontend\models\People;
use frontend\models\Typepoles;
use yii;

/**
 * This is the model class for table "reestr".
 *
 * @property integer $id
 * @property string $ENumber
 * @property string $OldNumber
 * @property integer $ManId
 * @property integer $orgId
 * @property integer $typePoles
 * @property string $beginDate
 * @property string $endDate
 * @property string $stmDate
 * @property integer $stmType
 * @property integer $formPoles
 * @property integer $reasonStm
 * @property string $numBlank
 * @property string $numCard
 * @property integer $statusPoles
 * @property string $AnnulDate
 * @property string $InsDate
 * @property integer $cr
 * @property string $crdate
 * @property string $crcomment
 * @property integer $PVCode
 * @property integer $reasonG
 *
 * @property ManualRequests[] $manualRequests
 * @property People $man
 * @property Smo $org
 * @property Typepoles $typePoles0
 */
class ReestrBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reestr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ENumber', 'OldNumber', 'numBlank', 'numCard', 'crcomment'], 'string'],
            [['ManId', 'orgId', 'typePoles', 'statusPoles'], 'required'],
            [['ManId', 'orgId', 'typePoles', 'stmType', 'formPoles', 'reasonStm', 'statusPoles', 'cr', 'PVCode', 'reasonG'], 'integer'],
            [['beginDate', 'endDate', 'stmDate', 'AnnulDate', 'InsDate', 'crdate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ENumber' => 'Номер',
            'OldNumber' => 'Старый номер',
            'ManId' => 'Man ID',
            'orgId' => 'Org ID',
            'typePoles' => 'Тип полиса',
            'beginDate' => 'Дата начала',
            'endDate' => 'Дата окончания',
            'stmDate' => 'Дата заявления',
            'stmType' => 'Тип заявления',
            'formPoles' => 'Форма полиса',
            'reasonStm' => 'Причина',
            'numBlank' => 'Номер бланка',
            'numCard' => 'Номер карты',
            'statusPoles' => 'Статус',
            'AnnulDate' => 'Дата аннулирования',
            'InsDate' => 'Дата добавления',
            'cr' => 'Cr',
            'crdate' => 'Crdate',
            'crcomment' => 'Crcomment',
            'PVCode' => 'Pvcode',
            'reasonG' => 'Reason G',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManualRequests()
    {
        return $this->hasMany(ManualRequests::className(), ['reestrid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMan()
    {
        return $this->hasOne(People::className(), ['id' => 'ManId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(Smo::className(), ['id' => 'orgId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypePoles0()
    {
        return $this->hasOne(Typepoles::className(), ['id' => 'typePoles']);
    }
    public function afterFind()
    {
        parent::afterFind();
        foreach ($this->getTableSchema()->columns as $col) {
            if ($col->type == 'datetime') {
                $attr = $col->name;
                if (empty($this->$attr) or $this->$attr == '1900-01-01')
                    $this->$attr = null;
                else
                    $this->$attr = Yii::$app->formatter->asDate($this->$attr, 'dd.MM.Y');
            }
        }
    }
}
