<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.07.2016
 * Time: 14:35
 */

namespace common\components;


class MyXmlReader extends \SimpleXMLReader
{
    public $xmlerrors = array();

    public function parse()
    {
        libxml_clear_errors();
        libxml_use_internal_errors(true);
        parent::parse();
        $this->xmlerrors = libxml_get_errors();
    }

    /**
     * @param string $xmlFilename Path to the XML file
     * @param string $version 1.0
     * @param string $encoding utf-8
     * @return bool
     */
    public function isXMLFileValid($xmlFilename, $version = '1.0', $encoding = 'windows-1251')
    {
        $xmlContent = file_get_contents($xmlFilename);
        return $this->isXMLContentValid($xmlContent, $version, $encoding);
    }

    /**
     * @param string $xmlContent A well-formed XML string
     * @param string $version 1.0
     * @param string $encoding utf-8
     * @return bool
     */
    public function isXMLContentValid($xmlContent, $version = '1.0', $encoding = 'utf-8')
    {
        if (trim($xmlContent) == '') {
            return false;
        }

        libxml_use_internal_errors(true);

        $doc = new \DOMDocument($version, $encoding);
        $doc->loadXML($xmlContent);

        $errors = libxml_get_errors();
        libxml_clear_errors();

        return $errors;
    }
}