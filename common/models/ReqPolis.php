<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "req_polis".
 *
 * @property integer $id
 * @property integer $rid
 * @property string $okato
 * @property string $ogrn
 * @property string $datebegin
 * @property string $dateend
 * @property string $typepoles
 * @property string $enumber
 * @property integer $prev
 */
class ReqPolis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'req_polis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rid'], 'required'],
            [['id', 'rid', 'prev'], 'integer'],
            [['okato', 'ogrn', 'typepoles', 'enumber'], 'string'],
            [['datebegin', 'dateend'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rid' => 'RequestsID',
            'okato' => 'ОКАТО',
            'ogrn' => 'ОГРН',
            'datebegin' => 'Дата начала',
            'dateend' => 'Дата окончания',
            'typepoles' => 'Тип полиса',
            'enumber' => 'Номер полиса',
            'prev' => 'Предыдущий',
        ];
    }
}
