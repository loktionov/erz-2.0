<?php
/**
 * Application configuration shared by all applications and test types
 */
return [
    'components' => [
        'db' => [
            'dsn' => 'sqlsrv:Server=srv-registr\sql;Database=yii2_advanced_tests',
            'username' => 'sa',
            'password' => '5FKMgj,uyj12#',
        ],
        'mailer' => [
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
];
