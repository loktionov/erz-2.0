<?php

namespace frontend\models;

use Yii;
use yii\db\mssql\PDO;

/**
 * This is the model class for table "PMO".
 *
 * @property integer $KODPMO
 * @property integer $KODMO
 * @property string $NAMPMO
 * @property string $ADRESPMO
 */
class PMO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PMO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KODPMO'], 'required'],
            [['KODPMO', 'KODMO'], 'integer'],
            [['NAMPMO', 'ADRESPMO'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'KODPMO' => 'Kodpmo',
            'KODMO' => 'Kodmo',
            'NAMPMO' => 'Nampmo',
            'ADRESPMO' => 'Adrespmo',
        ];
    }
    public static function GetSelectizeArray(){

        return Yii::$app->db->createCommand("select kodpmo value, nampmo text, concat(kodmo,': ',nammo, ' (', adrespmo ,')') cat from pmo_view")->queryAll();

    }
    public static function GetSelectizeOption($pmo){
        return Yii::$app->db->
            createCommand(
                "select kodpmo, nampmo from pmo_view where kodpmo=:pmo",
                [':pmo'=>$pmo]
            )
            ->queryOne(PDO::FETCH_KEY_PAIR);
    }
}
