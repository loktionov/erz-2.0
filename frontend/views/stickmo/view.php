<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StickMO */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Stick Mos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stick-mo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'peopleid',
            'PodMOCode',
            'MOCode',
            'DateBegin',
            'DateEnd',
            'Reason',
            'UchCode',
            'DocCode',
            'StickDate',
        ],
    ]) ?>

</div>
