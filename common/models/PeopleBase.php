<?php

namespace common\models;

use common\components\NameValidator;
use frontend\models\Reestr;
use yii;

/**
 * This is the model class for table "people".
 *
 * @property integer $id
 * @property string $ENP
 * @property string $Name
 * @property string $sName
 * @property string $pName
 * @property string $dateMan
 * @property integer $sexMan
 * @property string $pbMan
 * @property string $nationMan
 * @property integer $typeDoc
 * @property string $serDoc
 * @property string $numDoc
 * @property string $dateDoc
 * @property integer $statusMan
 * @property integer $regKladrId
 * @property string $addressReg
 * @property string $dateReg
 * @property string $addressLive
 * @property integer $activPolesId
 * @property string $dateDeath
 * @property integer $liveKladrId
 * @property string $snils
 * @property string $okato_mj
 * @property string $InsDate
 * @property integer $cr
 * @property string $crdate
 * @property integer $lostman
 * @property string $proxy_man
 * @property string $contact
 * @property string $doc_date_end
 * @property integer $livetemp
 * @property integer $refugee
 * @property string $orgdoc
 * @property integer $FFOMS
 * @property integer $wid
 * @property string $okato_reg
 * @property string $kladr_reg
 * @property string $kladr_live
 * @property integer $pr_rab
 *
 * @property Documents[] $documents
 * @property ManualRequests[] $manualRequests
 * @property PeopleAudit[] $peopleAudits
 * @property Reestr[] $reestrs
 * @property RequestsBookmark[] $requestsBookmarks
 */
class PeopleBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ENP', 'Name', 'sName', 'pName', 'pbMan', 'nationMan', 'serDoc', 'numDoc', 'addressReg', 'addressLive', 'snils', 'okato_mj', 'proxy_man', 'contact', 'orgdoc', 'okato_reg', 'kladr_reg', 'kladr_live'], 'string'],
            [['Name', 'sName', 'dateMan', 'sexMan', 'numDoc'], 'required'],
            [['dateMan', 'dateDoc', 'dateReg', 'dateDeath', 'InsDate', 'crdate', 'doc_date_end'], 'safe'],
            [['sexMan', 'typeDoc', 'statusMan', 'regKladrId', 'activPolesId', 'liveKladrId', 'cr', 'lostman', 'livetemp', 'refugee', 'FFOMS', 'wid', 'pr_rab'], 'integer'],
            [['sName', 'Name', 'pName'], NameValidator::className()],
            ['id', 'docValidate']
        ];
    }

    public function docValidate($attr)
    {
        $i = 0;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ENP' => 'ЕНП',
            'Name' => 'Имя',
            'sName' => 'Фамилия',
            'pName' => 'Отчество',
            'dateMan' => 'Дата рождения',
            'sexMan' => 'Пол',
            'pbMan' => 'Место рождения',
            'nationMan' => 'Гражданство',
            'typeDoc' => 'Тип документа',
            'serDoc' => 'Серия документа',
            'numDoc' => 'Номер документа',
            'dateDoc' => 'Дата выдачи документа',
            'statusMan' => 'Занятость',
            'regKladrId' => 'Reg Kladr ID',
            'addressReg' => 'Адрес регистрации',
            'dateReg' => 'Дата регистрации',
            'addressLive' => 'Адрес проживания',
            'activPolesId' => 'Activ Poles ID',
            'dateDeath' => 'Дата смерти',
            'liveKladrId' => 'Live Kladr ID',
            'snils' => 'СНИЛС',
            'okato_mj' => 'Okato Mj',
            'InsDate' => 'Ins Date',
            'cr' => 'Cr',
            'crdate' => 'Crdate',
            'lostman' => 'Активность',
            'proxy_man' => 'Proxy Man',
            'contact' => 'Контакты',
            'doc_date_end' => 'Дата окончания документа',
            'livetemp' => 'Livetemp',
            'refugee' => 'Refugee',
            'orgdoc' => 'Орган, выдавший документ',
            'FFOMS' => 'Ffoms',
            'wid' => 'Wid',
            'okato_reg' => 'Okato Reg',
            'kladr_reg' => 'Kladr Reg',
            'kladr_live' => 'Kladr Live',
        ];
    }


    public function getReestrs()
    {
        return $this->hasMany(Reestr::className(), ['ManId' => 'id']);
    }


    public function getActiveReestr()
    {
        $model = ReestrBase::find()->where('manid=:id and statusPoles  > 0', [':id' => $this->id])->one();
        return empty($model) ? new ReestrBase() : $model;
    }

    public function getActiveDocument()
    {
        $model = Documents::find()->where('peopleid=:id and actual > 0', [':id' => $this->id])->one();
        return empty($model) ? new Documents() : $model;
    }

    public function getStickMo()
    {
        $model = StickMo::find()->where('peopleid=:id', [':id' => $this->id])->one();
        return empty($model) ? new StickMo() : $model;
    }

    public function getProxy()
    {
        $model = Proxy::find()->where('peopleid=:id', [':id' => $this->id])->one();
        return empty($model) ? new Proxy() : $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['peopleid' => 'id']);
    }

    public function getAddress($type)
    {
        $model = Address::find()->where('peopleid=:id AND actual=1 AND addr_type=:type', [':id' => $this->id, ':type' => $type])->one();
        return empty($model) ? new Address() : $model;
    }

    public function getRegAddress()
    {
        return $this->getAddress('reg');
    }

    public function getLiveAddress()
    {
        return $this->getAddress('live');
    }

    public function afterFind()
    {
        parent::afterFind();
        foreach ($this->getTableSchema()->columns as $col) {
            if ($col->type == 'datetime') {
                $attr = $col->name;
                if (empty($this->$attr) or $this->$attr == '1900-01-01')
                    $this->$attr = null;
                else
                    $this->$attr = Yii::$app->formatter->asDate($this->$attr, 'dd.MM.Y');
            }
        }
    }

    public function updateAddress()
    {
        $addr_reg = PeopleAddress::findOne(['peopleid' => $this->id, 'actual' => 1, 'addr_type' => ['reg']]);
        $addr_reg = empty($addr_reg) ? new PeopleAddress() : $addr_reg;
        $addr_reg->regdate = $this->dateReg;
        $addr_reg->attributes = PeopleAddress::getAddress($this->id, 'reg', $this->okato_reg, $this->kladr_reg, $this->addressReg);
        if (empty($addr_reg->fias)) {
            $addr_reg->setFiasByKladr();
        }
        $addr_reg->getAddressCodeByKladr();
        $addr_reg->save();

        $addr_live = PeopleAddress::findOne(['peopleid' => $this->id, 'actual' => 1, 'addr_type' => ['live']]);
        $addr_live = empty($addr_live) ? new PeopleAddress() : $addr_live;
        $addr_live->regdate = $this->dateReg;
        $addr_live->attributes = PeopleAddress::getAddress($this->id, 'live', $this->okato_mj, $this->kladr_live, $this->addressLive);
        if (empty($addr_live->fias)) {
            $addr_live->setFiasByKladr();
        }
        $addr_live->getAddressCodeByKladr();
        $addr_live->save();

        if (PeopleAddress::isEqual($addr_reg, $addr_live)) {
            //$addr_live = new PeopleAddress();
        }
        return ['reg' => $addr_reg, 'live' => $addr_live];
    }
}
