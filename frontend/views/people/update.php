<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\People */

$this->title = 'Обновить данные: ' . ' ' . implode(' ', [$model->sName, $model->Name, $model->pName]);
$this->params['breadcrumbs'][] = ['label' => 'Peoples', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="people-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'regAddress' => $regAddress,
        'liveAddress' => $liveAddress,
        'proxy' => $proxy,
    ]) ?>

</div>
