<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 22.07.2016
 * Time: 17:53
 */

namespace common\components;


use yii\validators\EmailValidator;

class EmailsValidator extends EmailValidator
{
    public function validateAttribute($model, $attribute)
    {
        $emails = explode(';', $model->$attribute);
        foreach ($emails as $value) {
            $res = $this->validateValue(trim($value));
            if (isset($res))
                $model->addError($attribute, 'Ошибка в адресе электронной почты ' . $value);
        }
    }
}