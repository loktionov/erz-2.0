<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.07.2016
 * Time: 17:08
 */

namespace common\components;


use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\validators\Validator;

class ConsoleActiveRecord extends ActiveRecord
{
    public $errorsEx = [];
    const SCENARIO_LINK = 'link';

    /**
     * Performs the data validation.
     *
     * This method executes the validation rules applicable to the current [[scenario]].
     * The following criteria are used to determine whether a rule is currently applicable:
     *
     * - the rule must be associated with the attributes relevant to the current scenario;
     * - the rules must be effective for the current scenario.
     *
     * This method will call [[beforeValidate()]] and [[afterValidate()]] before and
     * after the actual validation, respectively. If [[beforeValidate()]] returns false,
     * the validation will be cancelled and [[afterValidate()]] will not be called.
     *
     * Errors found during the validation can be retrieved via [[getErrors()]],
     * [[getFirstErrors()]] and [[getFirstError()]].
     *
     * @param array $attributeNames list of attribute names that should be validated.
     * If this parameter is empty, it means any attribute listed in the applicable
     * validation rules should be validated.
     * @param boolean $clearErrors whether to call [[clearErrors()]] before performing validation
     * @param bool $skipOnModelHasError
     * @return bool whether the validation is successful without any error.
     */
    public function validate($attributeNames = null, $clearErrors = true, $skipOnModelHasError = false)
    {
        if ($clearErrors) {
            $this->clearErrors();
        }

        if (!$this->beforeValidate()) {
            return false;
        }

        $scenarios = $this->scenarios();
        $scenario = $this->getScenario();
        if (!isset($scenarios[$scenario])) {
            throw new InvalidParamException("Unknown scenario: $scenario");
        }

        if ($attributeNames === null) {
            $attributeNames = $this->activeAttributes();
        }

        foreach ($this->getActiveValidators() as $validator) {
            if ($this->hasErrors() AND $skipOnModelHasError)
                break;
            $errors = $this->getErrors();
            $validator->validateAttributes($this, $attributeNames);
            $this->setErrorEx($errors, $validator);
        }
        $this->afterValidate();

        return !$this->hasErrors();
    }

    public function setErrorEx($prev, $validator)
    {
        if (!$this->hasErrors())
            return;
        $errors = $this->errors;
        foreach ($errors as $a => $v) {
            if (!empty($prev[$a]))
                $diff = array_diff($errors[$a], $prev[$a]);
            else
                $diff = $errors[$a];
            foreach ($diff as $k => $e) {
                $validatorName = str_replace('validator', '', strtolower(basename(get_class($validator))));
                if ($validatorName == 'inline') {
                    $validatorName = strtolower($validator->method);
                }
                $this->errorsEx[$a][$validatorName] = $e;
            }
        }
    }
}