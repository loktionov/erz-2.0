<?php

namespace frontend\controllers;

use frontend\models\MO;
use frontend\models\People;
use frontend\models\PMO;
use Yii;
use frontend\models\StickMO;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StickMOController implements the CRUD actions for StickMO model.
 */
class StickmoController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all StickMO models.
     * @return mixed
     */
    private function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => StickMO::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StickMO model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StickMO model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = StickMO::findOne(['peopleid' => $id]);
        if (!empty($model))
            return $this->redirect(['update', 'id' => $id]);
        $model = new StickMO();
        $model->peopleid = $id;
        $model->okato = '07000';
        /** @var $people People */
        $people = People::findOne($id);
        $fio = $people->sName . ' ' . $people->Name . ' ' . $people->pName;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->peopleid]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'fio' => $fio,
            ]);
        }
    }

    /**
     * Updates an existing StickMO model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = StickMO::findOne(['peopleid' => $id]);
        if (empty($model))
            return $this->redirect(['create', 'id' => $id]);
        /** @var $people People */
        $people = People::findOne($model->peopleid);
        $fio = $people->sName . ' ' . $people->Name . ' ' . $people->pName;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->peopleid]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'fio' => $fio,
            ]);
        }
    }

    /**
     * Deletes an existing StickMO model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StickMO model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StickMO the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StickMO::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetpmo($kodmo)
    {
        $data = ArrayHelper::map(PMO::find()->where("KODMO=:MO", [':MO' => $kodmo])->all(), 'KODPMO', 'NAMPMO');
        $res = [];
        if (count($data) > 0) {
            foreach ($data as $pmokod => $pmoname) {
                //echo "<option value='" . $pmokod . "'>" . $pmoname . "</option>";
                $res[$pmokod] = $pmoname;
            }
        }
        echo json_encode($res);
    }

    public function actionGetmo($okato)
    {
        $okato = mb_substr($okato, 0, 2);
        $stav = ($okato == '07') ? ' AND mo_type = 1' : '';
        $data = MO::find()->where('substring(KODOKATO, 1, 2) = :OKATO' . $stav, [':OKATO' => $okato])
            ->select(["KODMO", "NAMMO" => new Expression("CONCAT(KODMO,': ',NAMMO)")])
            ->all();
        $data = ArrayHelper::map($data, 'KODMO', 'NAMMO');
        $res = [];
        if (count($data) > 0) {
            foreach ($data as $mokod => $moname) {
                //echo "<option value='" . $mokod . "'>" . $moname . "</option>";
                $res[$mokod] = $moname;
            }
        }
        echo json_encode($res);
    }
}
