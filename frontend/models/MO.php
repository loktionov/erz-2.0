<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 28.10.2016
 * Time: 10:22
 */

namespace frontend\models;

use common\models\MoBase;

class MO extends MoBase
{

    public function getOkato()
    {
        return $this->hasOne(OKATO::className(), ["OKATO" => "KODOKATO"]);
    }

}