<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 03.06.2016
 * Time: 17:07
 */

namespace common\components;


use yii\base\Model;
use yii\validators\StringValidator;

class NameValidator extends StringValidator
{

    private function getRegexp()
    {
        return [
            '/^([A-Z\d][A-Za-z\d\.\-\s]*)|([А-ЯЁ\d][А-ЯЁа-яё\d\.\-\s]*)$/iu' => true,
            '/\.\s*\-|\.\s*\.|\-\s*\-|\-\s*\./iu' => false,
            '/^[A-ZА-ЯЁa-zа-яё\d\s-]+$/iu' => true,
        ];
    }

    /**
     * @@inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        parent::validateAttribute($model, $attribute);
        if ($model->hasErrors($attribute))
            return;

        foreach ($this->getRegexp() as $q => $match) {
            if (!preg_match($q, $model->$attribute) == $match) {
                $this->addFormatError($model, $attribute);
                return;
            }
        }
        return;
    }

    /**
     * @param $model Model
     * @param $attribute string
     */
    private function addFormatError($model, $attribute)
    {
        $model->addError($attribute, "Неверный формат поля «{$model->getAttributeLabel($attribute)}».");
    }


}