<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ref_type_doc".
 *
 * @property integer $id
 * @property string $Name
 * @property integer $SpecialMed
 * @property string $DateBegin
 * @property string $DateEnd
 */
class RefUchastok extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_type_doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'SpecialMed'], 'integer'],
            [['Name'], 'string'],
            [['DateBegin', 'DateEnd'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'SpecialMed' => 'Special Med',
            'DateBegin' => 'Date Begin',
            'DateEnd' => 'Date End',
        ];
    }
}
