<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property integer $peopleid
 * @property integer $typedoc
 * @property string $serdoc
 * @property string $numdoc
 * @property string $datedoc
 * @property string $orgdoc
 * @property integer $actual
 * @property string $doc_date_end
 * @property string $insdate
 *
 * @property People $people
 * @property Typedocument $typedoc0
 */
class Documents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['peopleid', 'typedoc'], 'required'],
            [['peopleid', 'typedoc', 'actual'], 'integer'],
            [['serdoc', 'numdoc', 'orgdoc'], 'string'],
            [['datedoc', 'doc_date_end', 'insdate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'peopleid' => 'Peopleid',
            'typedoc' => 'Typedoc',
            'serdoc' => 'Serdoc',
            'numdoc' => 'Numdoc',
            'datedoc' => 'Datedoc',
            'orgdoc' => 'Orgdoc',
            'actual' => 'Actual',
            'doc_date_end' => 'Doc Date End',
            'insdate' => 'Insdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasOne(People::className(), ['id' => 'peopleid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypedoc0()
    {
        return $this->hasOne(Typedocument::className(), ['id' => 'typedoc']);
    }
}
