<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "disp_os".
 *
 * @property integer $id
 * @property string $disp_os
 */
class DispOs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_os';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disp_os'], 'required'],
            [['disp_os'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'disp_os' => 'Disp Os',
        ];
    }
}
