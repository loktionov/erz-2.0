<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "MO".
 *
 * @property string $KODMO
 * @property string $KODOKATO
 * @property string $NAMMO
 * @property string $ADRESMO
 * @property integer $mo_type
 */
class MoBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['KODMO'], 'required'],
            [['KODMO', 'KODOKATO', 'NAMMO', 'ADRESMO'], 'string'],
            [['mo_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'KODMO' => 'Kodmo',
            'KODOKATO' => 'Kodokato',
            'NAMMO' => 'Nammo',
            'ADRESMO' => 'Adresmo',
            'mo_type' => 'Mo Type',
        ];
    }
}
