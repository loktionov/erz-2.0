<?php

use yii\bootstrap\Collapse;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\MyHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\People */
/* @var $regAddress \common\models\Address */
/* @var $liveAddress \common\models\Address */
/* @var $proxy \common\models\Proxy */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .help-block {
        min-height: 20px;
    }

    .form-control {
        width: 300px;
    }

    .collumn-left-50 {
        float: left;
        width: 50%
    }

    .cell {
        float: left;
        width: 200px;
        margin-right: 10px;
    }

    .cell .form-control {

        width: 200px;
    }

    .sub-cell .form-control {
        width: 127px;
        text-align: center;
    }

    .sub-cell {
        width: 150px;
        margin-left: 15px;
    }

    .sub-cell3 .form-control {
        width: 70px;
        text-align: center;
    }

    .sub-cell3 {
        width: 102px;
        margin-left: 15px;
        margin-right: 0;
    }

    .panel-group {
        margin-left: -25px;
    }

    .panel-body {
        border: none;
    }

    .panel-default {
        border: none;
    }

    .select2-container {
        width: 300px;
    }
</style>
<div class="people-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => ['class' => 'form-horizontal'],
            /*'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n<div class='pull-left'>{input}</div>\n<div class='pull-left'>{error}</div>\n<div style='clear: both;'></div>{hint}\n{endWrapper}",
            ],*/
        ]); ?>


    <br/>
    <fieldset>
        <legend><?= $model->ENP ?></legend>
        <div class="row">
            <div class="cell"><?= $form->field($model, 'sName')->textInput() ?></div>

            <div class="cell"> <?= $form->field($model, 'Name')->textInput() ?></div>

            <div class="cell"> <?= $form->field($model, 'pName')->textInput() ?></div>

            <div class="cell"><?= $form->field($model, 'dateMan', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
                    ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999']) ?></div>
            <div class="cell">
                <div class="form-inline">
                    <?= $form->field($model, 'sexMan')->radioList(MyHelper::getSexArray()) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="cell"><?= $form->field($model, 'nationMan')->dropDownList(

                    \common\models\Nation::find()->select(['NAME11', 'ALFA3'])->indexBy('ALFA3')->orderBy('NAME11')->column(),
                    ['prompt' => 'Выберите гражданство']

                ) ?></div>
            <div class="cell">
                <?= $form->field($model, 'snils')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999-999-999 99']) ?>
            </div>
            <div class="cell"><?= $form->field($model, 'pbMan')->textInput() ?></div>
            <div class="cell"><?= $form->field($model, 'contact')->textInput() ?></div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Документы</legend>
        <div class="row">
            <div class="collumn-left-50">
                <?= $form->field($model, 'typeDoc')->dropDownList(

                    \common\models\Typedoc::find()->select(['Name', 'id'])->indexBy('id')->orderBy('Name')->column(),
                    ['prompt' => 'Выберите документ']

                ) ?>

                <div class="row form-group">
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'serDoc')->textInput()->label("Серия") ?>
                    </div>
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'numDoc')->textInput()->label("Номер") ?>
                    </div>
                </div>

                <?= $form->field($model, 'orgdoc')->textInput() ?>
                <div class="row">
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'dateDoc', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
                            ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999'])->label("Выдан") ?>
                    </div>
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'doc_date_end', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
                            ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999'])->label('Окончание') ?>
                    </div>
                </div>

            </div>
            <div class="collumn-left-50">
                <?= $form->field($model, 'typeDoc')->dropDownList(

                    \common\models\Typedoc::find()->select(['Name', 'id'])->indexBy('id')->orderBy('Name')->column(),
                    ['prompt' => 'Выберите документ']

                ) ?>
                <div class="row form-group">
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'serDoc')->textInput()->label("Серия") ?>
                    </div>
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'numDoc')->textInput()->label("Номер") ?>
                    </div>
                </div>
                <?= $form->field($model, 'orgdoc')->textInput() ?>
                <div class="row form-group">
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'dateDoc', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
                            ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999'])->label("Выдан") ?>
                    </div>
                    <div class="cell sub-cell">
                        <?= $form->field($model, 'doc_date_end', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
                            ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999'])->label('Окончание') ?>
                    </div>
                </div>

            </div>

        </div>
    </fieldset>
    <div style="clear: both"></div>
    <fieldset>
        <legend>Адрес</legend>

        <div class="collumn-left-50">
            <?= $form->field($model, 'dateReg', [])->widget(\yii\jui\DatePicker::className(), ['language' => 'ru', 'dateFormat' => 'dd-MM-yyyy', 'class' => 'form-control'])
                ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999']) ?>

            <?= $this->render("_address", ['type' => "reg", 'model' => $regAddress, 'form' => $form]) ?>

        </div>
        <div class="collumn-left-50">

            <div style="margin: 31px 0 49px 84px">
                <label for="ch1" id="ch1-label"> Совпадает </label>
                <input type="checkbox" name="ch1" id="ch1" checked value="1">
            </div>
            <?= $this->render("_address", ['type' => "live", 'model' => $liveAddress, 'form' => $form]) ?>
        </div>

    </fieldset>
</div>


<?php


?>


<fieldset>

    <?= Collapse::widget([
        'encodeLabels' => false,
        'items' => [
            // equivalent to the above
            [
                'label' => '<legend>Представитель</legend>',
                'content' => $this->render("_proxy_form", ['proxy' => $proxy, 'model' => $model, 'form' => $form]),
                // open its content by default
                'contentOptions' => ['class' => 'in']
            ],
        ]
    ]); ?>
</fieldset>


<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary pull-right']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
