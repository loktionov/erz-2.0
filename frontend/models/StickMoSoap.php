<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 30.12.2015
 * Time: 10:55
 */

namespace frontend\models;

use common\models\StickMo;

class StickMoSoap extends StickMo
{
    public function rules()
    {
        return [
            [['MOCode', 'Reason'], 'safe']
        ];
    }
    /**
     * @var string
     * @soap
     */
    public $MOCode;

    /**
     * @var string
     * @soap
     */
    public $Reason;

}