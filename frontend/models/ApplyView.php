<?php

namespace frontend\models;

use frontend\controllers\DispController;
use Yii;

/**
 * This is the model class for table "apply_view".
 *
 * @property integer $id
 * @property integer $peopleid
 * @property string $smo
 * @property string $sName
 * @property string $Name
 * @property string $pName
 * @property integer $pp
 * @property integer $reasonStm
 * @property integer $formPoles
 * @property integer $stmType
 * @property string $ENP
 * @property integer $sexMan
 * @property integer $statusMan
 * @property string $dateMan
 * @property string $pbMan
 * @property integer $typeDoc
 * @property string $serDoc
 * @property string $numDoc
 * @property string $dateDoc
 * @property string $doc_date_end
 * @property string $doc
 * @property string $orgdoc
 * @property string $nationMan
 * @property string $nation
 * @property integer $typedoc_ino
 * @property string $serdoc_ino
 * @property string $numdoc_ino
 * @property string $datedoc_ino
 * @property string $doc_date_end_ino
 * @property string $doc_ino
 * @property string $orgdoc_ino
 * @property string $snils
 * @property string $contact
 * @property string $sname_pp
 * @property string $name_pp
 * @property string $pname_pp
 * @property string $relation_pp
 * @property string $doc_pp
 * @property string $serdoc_pp
 * @property string $numdoc_pp
 * @property string $datedoc_pp
 * @property string $contact_pp
 * @property string $tel2
 * @property string $doveren_pp
 * @property string $contacts_pp
 * @property string $uved_pp
 * @property integer $stmt
 * @property string $rsn
 * @property integer $r1
 * @property integer $r2
 * @property integer $r3
 * @property integer $r4
 * @property string $stmdate
 * @property string $temp_polis
 * @property string $smo_addr
 * @property string $enddate
 * @property string $from
 */
class ApplyView extends \yii\db\ActiveRecord
{
    /**
     * @var Doc
     */
    public $docobj_ino;
    /**
     * @var Doc
     */
    public $docobj_eaes;
    /**
     * @var Doc
     */
    public $docobj_dogovor;

    public function setDocobj()
    {
        $newdoc = new Doc();
        $this->docobj_dogovor = $newdoc;
        $this->docobj_eaes = new $newdoc;
        $this->docobj_ino = new $newdoc;

        $doc = new Doc();
        $doc->doc = $this->doc_ino;
        $doc->datedoc = strtotime($this->datedoc_ino) !== false ? date('d.m.Y', strtotime($this->datedoc_ino)) : '';
        $doc->doc_date_end = strtotime($this->doc_date_end_ino) !== false ? date('d.m.Y', strtotime($this->doc_date_end_ino)) : '';
        $doc->serdoc = $this->serdoc_ino;
        $doc->numdoc = $this->numdoc_ino;
        $doc->orgdoc = $this->orgdoc_ino;
        $doc->typedoc = $this->typedoc_ino;
        if (in_array($this->typedoc_ino, [11, 23])) {
            $this->docobj_ino = $doc;
        } else if (in_array($this->typedoc_ino, [26])) {
            $this->docobj_eaes = $doc;
        } else if (in_array($this->typedoc_ino, [29])) {
            $this->docobj_dogovor = $doc;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apply_view';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function getFIO()
    {
        return implode(' ', [$this->sName, $this->Name, $this->pName]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['smo', 'sName', 'Name', 'pp', 'ENP', 'sexMan', 'dateMan', 'stmt'], 'required'],
            [['smo', 'sName', 'Name', 'pName', 'ENP', 'pbMan', 'serDoc', 'numDoc', 'doc', 'nationMan', 'nation', 'serdoc_ino', 'numdoc_ino', 'doc_ino', 'snils', 'contact', 'sname_pp', 'name_pp', 'pname_pp', 'relation_pp', 'doc_pp', 'serdoc_pp', 'numdoc_pp', 'datedoc_pp', 'contact_pp', 'tel2', 'doveren_pp', 'contacts_pp', 'uved_pp'], 'string'],
            [['pp', 'reasonStm', 'formPoles', 'stmType', 'sexMan', 'statusMan', 'typeDoc', 'typedoc_ino', 'stmt'], 'integer'],
            [['dateMan', 'dateDoc', 'doc_date_end', 'datedoc_ino', 'doc_date_end_ino'], 'safe'],
            [['rsn'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'smo' => 'Smo',
            'sName' => 'S Name',
            'Name' => 'Name',
            'pName' => 'P Name',
            'pp' => 'Pp',
            'reasonStm' => 'Reason Stm',
            'formPoles' => 'Form Poles',
            'stmType' => 'Stm Type',
            'ENP' => 'Enp',
            'sexMan' => 'Sex Man',
            'statusMan' => 'Status Man',
            'dateMan' => 'Date Man',
            'pbMan' => 'Pb Man',
            'typeDoc' => 'Type Doc',
            'serDoc' => 'Ser Doc',
            'numDoc' => 'Num Doc',
            'dateDoc' => 'Date Doc',
            'doc_date_end' => 'Doc Date End',
            'doc' => 'Doc',
            'nationMan' => 'Nation Man',
            'nation' => 'Nation',
            'typedoc_ino' => 'Typedoc Ino',
            'serdoc_ino' => 'Serdoc Ino',
            'numdoc_ino' => 'Numdoc Ino',
            'datedoc_ino' => 'Datedoc Ino',
            'doc_date_end_ino' => 'Doc Date End Ino',
            'doc_ino' => 'Doc Ino',
            'snils' => 'Snils',
            'contact' => 'Contact',
            'sname_pp' => 'Sname Pp',
            'name_pp' => 'Name Pp',
            'pname_pp' => 'Pname Pp',
            'relation_pp' => 'Relation Pp',
            'doc_pp' => 'Doc Pp',
            'serdoc_pp' => 'Serdoc Pp',
            'numdoc_pp' => 'Numdoc Pp',
            'datedoc_pp' => 'Datedoc Pp',
            'contact_pp' => 'Contact Pp',
            'tel2' => 'Tel2',
            'doveren_pp' => 'Doveren Pp',
            'contacts_pp' => 'Contacts Pp',
            'uved_pp' => 'Uved Pp',
            'stmt' => 'Stmt',
            'rsn' => 'Rsn',
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->r1 = empty($this->r1) ? '' : 'check';
        $this->r2 = empty($this->r2) ? '' : 'check';
        $this->r3 = empty($this->r3) ? '' : 'check';
        $this->r4 = empty($this->r4) ? '' : 'check';
        $this->setDocobj();
    }

    public static function SplitDate($date)
    {
        $d = "";
        $m = "";
        $y = "";
        $reg_query = "/^(\d\d).(\d\d).\d\d(\d\d)/i";
        if (!preg_match($reg_query, $date, $matches)) {
            return array($d, $m, $y);
        }
        $d = $matches[1];
        $m = $matches[2];
        $y = $matches[3];
        switch ($m) {
            case '01':
                $m = "января";
                break;
            case '02':
                $m = "февраля";
                break;
            case '03':
                $m = "марта";
                break;
            case '04':
                $m = "апреля";
                break;
            case '05':
                $m = "мая";
                break;
            case '06':
                $m = "июня";
                break;
            case '07':
                $m = "июля";
                break;
            case '08':
                $m = "августа";
                break;
            case '09':
                $m = "сентября";
                break;
            case '10':
                $m = "октября";
                break;
            case '11':
                $m = "ноября";
                break;
            case '12':
                $m = "декабря";
                break;
        }
        return [$d, $m, $y];
    }
}

class Doc
{
    public $doc;
    public $typedoc;
    public $serdoc;
    public $numdoc;
    public $orgdoc;
    public $datedoc;
    public $doc_date_end;
}
