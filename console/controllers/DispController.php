<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 22.07.2016
 * Time: 16:34
 */

namespace console\controllers;

use common\models\DispMapomsView;
use console\components\DispXmlReader;
use common\models\DispFiles;
use console\components\FfomsXmlReader;
use console\components\SnilsXmlReader;
use Yii;
use yii\console\Controller;
use yii\swiftmailer\Mailer;
use ZipArchive;

class DispController extends Controller
{
    //public $root_path = 'Z:\ERZ\DISP\\';
    const ROOT_PATH = 'z:\ERZ\DISP\\';
    const TO_MO = 'z:\ERZ\DISP\TO_MO\\';
    const TO_MO_1C = 'z:\1C\TO_MO\\';
    const flk = 'z:\ERZ\DISP\FROM_MO\flk\\';
    const result = 'z:\ERZ\DISP\FROM_MO\result\\';
    const TO_SMO = 'z:\ERZ\DISP\TO_SMO\\';
    const FROM_MO = 'z:\ERZ\DISP\FROM_MO\\';

    public function actionParse()
    {
        $path = self::FROM_MO;
        $process = $path . 'process\\';
        while (true) {
            $files = glob($path . '*.*');
            usort($files, function ($a, $b) {
                return filemtime($a) > filemtime($b);
            });
            foreach ($files as $f) {
                if (!file_exists($f))
                    continue;
                try {
                    rename($f, $process . basename($f));
                    $f = $process . basename($f);
                    echo date(' d-m-Y H:i:s') . ' ' . mb_strtoupper($f) . PHP_EOL;
                    $file = $this->getFile($f);
                    //$file = DispFiles::findOne(224);

                    if ($file->result == 1) {
                        $file->result = 2;
                        $file->save(false, ['result']);
                        echo date(' d-m-Y H:i:s') . ' PARSE' . PHP_EOL;
                        $file = $this->parseFile($file);
                        try {
                            unlink($file->path);
                        } catch (\Exception $e) {
                            $this->handleError($e);
                        }
                    }

                    if (!$file->hasErrors() and empty($file->errorsEx) and empty($file->getFlk()->count())) {
                        echo date(' d-m-Y H:i:s') . ' DOUBLES' . PHP_EOL;
                        $file->doublesCheck();
                        $file->countCheck();
                    }
                    if (empty($file->getFlk()->count())) {
                        echo date(' d-m-Y H:i:s') . ' IDENT' . PHP_EOL;
                        $file->identification();
                        $file->doublesCheckIdent();
                        $file->stickmo();
                    }
                    echo date(' d-m-Y H:i:s') . ' SEND_MO' . PHP_EOL;
                    if (!empty($file->getContentCount())) {
                        $file->result = 3;
                    } else $file->result = 4;
                    $file->save(false, ['result']);
                    $this->sendResult($file);
                    if ($file->result == 3) {
                        echo date(' d-m-Y H:i:s') . ' SEND_SMO' . PHP_EOL;
                        $this->sendSMO($file);
                    }
                    rename($f, $path . 'done\\' . basename($f));
                } catch (\Exception $e) {
                    $this->handleError($e);
                }
            }
        }
    }

    /**
     * @param $path
     * @return DispFiles
     */
    private function getFile($path)
    {
        $file = new DispFiles(['path' => $path]);
        if (!$file->validate(null, false, true)) {
            $errors = $file->errorsEx;
            $file->scenario = DispFiles::SCENARIO_ERROR;
            $file->result = 0;
            $file->save(false);
            $file->linkFlk($errors);
        } else {
            $file->result = 1;
            $file->save(false);
        }
        return $file;
    }

    /**
     * @param $file DispFiles
     * @return DispFiles
     */
    private function parseFile($file)
    {
        try {
            $xmlreader = new DispXmlReader($file);
            $errors = $xmlreader->isXMLFileValid($file->path);
            $error = "Содержимое архива не является корректным XML-файлом." . PHP_EOL;
            if (!empty($errors)) {

                foreach ($errors as $err) {
                    if ($err instanceof \LibXMLError) {
                        $file->linkFlk(['file' => ['xmlparse' => $error . $err->message . PHP_EOL . 'строка: ' . $err->line]]);
                    }
                }
                return $file;
            }
            $xmlreader->open($file->path);
            $xmlreader->parse();
            $xmlreader->close();
            foreach ($xmlreader->xmlerrors as $err) {
                if ($err instanceof \LibXMLError) {
                    $file->linkFlk(['file' => ['xmlparse' => $error . $err->message . PHP_EOL . 'строка: ' . $err->line]]);
                }
            }
            if (empty($file->getZglv()->all())) {
                $file->errorsEx['file']['zglv'] = 'Отсутствует эелемент ZGLV';
            }
            $file->linkFlk();
        } catch (\Exception $e) {
            $this->handleError($e);
            $file->linkFlk(['file' => ['xmlparse' => 'Ошибка разбора XML']]);
        }
        return $file;
    }

    /**
     * @param $file DispFiles
     */
    private function sendResult($file)
    {
        $flk_name = empty($file->y . $file->n) ? '' : '_' . $file->y . str_pad($file->n, 2, '0', STR_PAD_LEFT);
        $flk_name = 'FT' . $file->mo . $flk_name . '.XML';
        $tmp_path = self::ROOT_PATH . 'tmp';
        $tmp = tempnam($tmp_path, '');
        file_put_contents($tmp, $file->getFlkXml());
        $tmp_zip = tempnam($tmp_path, '');
        $zip = new ZipArchive;
        $res = $zip->open($tmp_zip, ZipArchive::CREATE);
        if ($res === TRUE) {
            $zip->addFile($tmp, $flk_name);
            $zip->close();
            //echo 'ZIP' . PHP_EOL;
        } else {
            echo 'ZIP_ERROR' . PHP_EOL;
        }

        if (!empty($file->mo)) {
            copy($tmp_zip, self::flk . basename($flk_name, 'XML') . 'OMS');
            copy($tmp_zip, self::TO_MO . basename($flk_name, 'XML') . 'OMS');
        }

        if (!empty($file->zglv) and !empty($file->zglv->email)) {
            /** @var Mailer $mailer */
            $mailer = Yii::$app->get('mailer');
            $mailer->useFileTransport = false;
            $addrs = explode(';', $file->zglv->email);
            $addrs[] = 'support@tfomssk.ru';
            try {
                $mailer->compose()
                    ->setFrom('support@tfomssk.ru')
                    ->setTo($addrs)
                    ->setSubject('Протокол ФЛК ' . $file->filename)
                    ->setTextBody('Файл с протоколом ФЛК находится в приложении' . PHP_EOL . 'Письмо сгенерировано автоматически. Не следует отвечать на него.')
                    ->attach($tmp_zip, ['fileName' => basename($flk_name, 'XML') . 'OMS', 'contentType' => 'application/octet-stream'])
                    ->send();
                //echo 'E-MAIL' . PHP_EOL;
            } catch (\Exception $e) {
                $this->handleError($e, false);
            }
        }

        unlink($tmp);
        unlink($tmp_zip);
    }

    /**
     * @param $file DispFiles
     * @param bool $update
     * @param string $suffix
     */
    private function sendSMO($file, $update = true, $suffix = "")
    {
        $f = $file->getSMOXml(self::ROOT_PATH . 'tmp');
        foreach ($f as $smo => $tmp) {
            $protocol_name = empty($file->y . $file->n) ? '' : '_' . $file->y . str_pad($file->n, 2, '0', STR_PAD_LEFT);
            $protocol_name = 'GT' . $file->mo . $protocol_name . '_' . $smo . $suffix . '.XML';
            $tmp_path = self::ROOT_PATH . 'tmp';
            $tmp_zip = tempnam($tmp_path, '');
            $zip = new ZipArchive;
            $res = $zip->open($tmp_zip, ZipArchive::CREATE);
            if ($res === TRUE) {
                $zip->addFile($tmp, $protocol_name);
                $zip->close();
                //echo 'ZIP' . PHP_EOL;
            } else {
                echo 'ZIP_ERROR' . PHP_EOL;
            }
            copy($tmp_zip, self::TO_SMO . basename($protocol_name, 'XML') . 'OMS');
            copy($tmp_zip, self::TO_MO_1C . basename($protocol_name, 'XML') . 'OMS');
            copy($tmp_zip, self::result . basename($protocol_name, 'XML') . 'OMS');
            unlink($tmp);
            unlink($tmp_zip);
        }
        if ($update) {
            $file->result = 5;
            $file->save(false, ['result']);
        }
    }

    public function actionSmo()
    {
        $file_list = \Yii::$app->db->createCommand("

        SELECT DISTINCT f.id FROM 
  (SELECT *, ROW_NUMBER() OVER (PARTITION BY mo ORDER BY f.result DESC, insdate DESC) rn  FROM [rmp].[dbo].[disp_files] f WHERE f.y=17) f
  RIGHT JOIN mo ON mo.KODMO=f.mo AND mo.mo_type=1
  JOIN disp_content c ON c.fid=f.id
  JOIN disp_ident i ON i.cid=c.id
  WHERE f.rn=1

        ")->queryAll();

        foreach ($file_list as $f) {
            $file = DispFiles::findOne($f['id']);
            $this->sendSMO($file, false, '_final');
        }
    }

    public function actionMo()
    {
        $file_list = \Yii::$app->db->createCommand("

        SELECT DISTINCT f.id FROM ( SELECT f.*, mo.KODMO, mo.nammo FROM  mo
  LEFT JOIN (SELECT *, ROW_NUMBER() OVER (PARTITION BY mo ORDER BY f.result DESC, insdate DESC) rn  FROM [rmp].[dbo].[disp_files] f WHERE f.y=17 AND f.result=5 )f ON f.mo=mo.KODMO
  AND f.rn=1 
  WHERE mo.mo_type=1) f
  JOIN disp_flk flk ON flk.fid=f.id
  

        ")->queryAll();

        foreach ($file_list as $f) {
            $file = DispFiles::findOne($f['id']);
            $this->sendResult($file);
        }
    }


    public function actionSendmo($fid)
    {
        $file = DispFiles::findOne($fid);
        $this->sendResult($file);
    }

    public function actionFfoms()
    {
        $n = \Yii::$app->db->createCommand("

        SELECT isnull(max(nomer_z), 0) FROM [!ffoms_worked]

        ")->queryScalar();
        echo $n . PHP_EOL;
        $xmlreader = new FfomsXmlReader($n);
        $xmlreader->open('d:\FFOMS_worked_19.08.2016\PFR2FOMS_161_001.xml');
        $xmlreader->parse();
        $xmlreader->close();
        $xmlreader->insertZl();

    }

    public function actionFfomssnils()
    {

        $files = glob('d:\FOMS\*.xml');
        $i = 0;
        foreach ($files as $f) {
            $i++;
            echo 'file ' . $i . ' from ' . count($files) . ' ' . $f . PHP_EOL;
            $xmlreader = new SnilsXmlReader();
            $xmlreader->open($f);
            $xmlreader->parse();
            $xmlreader->close();
            $xmlreader->insertZl();
        }

    }

    /**
     * @param $e \Exception
     * @param bool $echo
     */
    public function handleError($e, $echo = true)
    {
        $mes = $e->getMessage() . PHP_EOL
            . 'Файл: ' . $e->getFile() . ' строка: ' . $e->getLine() . PHP_EOL
            . $e->getTraceAsString() . PHP_EOL;
        yii::error($mes);
        echo $echo ? $mes : $e->getMessage();
    }
}