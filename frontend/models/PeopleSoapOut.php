<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.12.2015
 * Time: 12:36
 */

namespace frontend\models;


class PeopleSoapOut extends PeopleSoap
{
    const RESULT_NULL = '000';
    const RESULT_OK = '001';
    const RESULT_DOUBLE = '002';
    const RESULT_ERROR = '003';

    public static function getResultMessage($result)
    {
        switch ($result) {
            case self::RESULT_NULL:
                return 'Ничего не найдено';
            case self::RESULT_OK:
                return 'Найден один застрахованный';
            case self::RESULT_DOUBLE:
                return 'Найден более одного застрахованного';
            case self::RESULT_ERROR:
                return 'Ошибка в данных застрахованного';
            default:
                return 'Неизвестный результат';
        }
    }

    public static function getResultArray($result)
    {
        return [$result => self::getResultMessage($result)];
    }

    /**
     * @var string[]
     * @soap
     */
    public $result;

    /**
     * @var string
     * @soap
     */
    public $ENP;

    /**
     * @var integer
     * @soap
     */
    public $id;

    /**
     * @var string
     * @soap
     */
    public $pbMan;

    /**
     * @var string
     * @soap
     */
    public $nationMan;

    /**
     * @var string
     * @soap
     */
    public $dateDoc;

    /**
     * @var integer
     * @soap
     */
    public $statusMan;

    /**
     * @var string
     * @soap
     */
    public $addressReg;

    /**
     * @var string
     * @soap
     */
    public $dateReg;

    /**
     * @var string
     * @soap
     */
    public $addressLive;

    /**
     * @var string
     * @soap
     */
    public $okato_mj;

    /**
     * @var string
     * @soap
     */
    public $contact;

    /**
     * @var string
     * @soap
     */
    public $doc_date_end;

    /**
     * @var string
     * @soap
     */
    public $orgdoc;

    /**
     * @var string
     * @soap
     */
    public $okato_reg;

    /**
     * @var string
     * @soap
     */
    public $kladr_reg;

    /**
     * @var string
     * @soap
     */
    public $kladr_live;

    /**
     * @var frontend\models\ReestrSoap
     * @soap
     */
    public $reestr;

    /**
     * @var frontend\models\StickMoSoap
     * @soap
     */
    public $stickmo;

    /*
     * @var frontend\models\DocumentsSoap
     * @soap
     */
    //public $document;

    public function rules()
    {
        return [
            [
                [
                    'ENP', 'Name', 'sName', 'pName', 'dateMan', 'sexMan', 'typeDoc', 'serDoc', 'numDoc', 'snils', 'pbMan',
                    'nationMan', 'dateDoc', 'statusMan', 'addressReg', 'dateReg', 'addressLive', 'okato_mj', 'contact',
                    'doc_date_end', 'orgdoc', 'okato_reg', 'kladr_reg', 'kladr_live', 'id',
                ], 'safe'
            ]
        ];
    }

}