<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.12.2015
 * Time: 12:26
 */

namespace frontend\models;
use common\models\Documents;

/** @soap-indicator all */
class DocumentsSoap extends Documents
{

    /**
     * @var integer
     * @soap
     */
    public $id;

    /**
     * @var integer
     * @soap
     */
    public $peopleid;

    /**
     * @var integer
     * @soap
     */
    public $typedoc;

    /**
     * @var string
     * @soap
     */
    public $serdoc;

    /**
     * @var string
     * @soap
     */
    public $numdoc;

    /**
     * @var string
     * @soap
     */
    public $datedoc;

    /**
     * @var string
     * @soap
     */
    public $orgdoc;

    /**
     * @var integer
     * @soap
     */
    public $actual;

    /**
     * @var string
     * @soap
     */
    public $doc_date_end;

    /**
     * @var string
     * @soap
     */
    public $insdate;

}