<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.07.2016
 * Time: 15:57
 */

namespace console\components;

use SimpleXMLReader;

class FfomsXmlReader extends SimpleXMLReader
{

    public $zl = [];
    public $zl_count = 0;
    public $zl_inserted = 0;
    public $n_start = 0;

    public function __construct($n = 0)
    {
		$this->n_start = $n;
        // by xpath
        $this->registerCallback("/zl_list/zl", array($this, "callbackZL"));
        $this->registerCallback("/zl_list/zglv", array($this, "callbackZGLV"));
    }

    public function insertZl($info = true)
    {
        $i = count($this->zl);
        if ($i == 0) {
            return;
        }
        \Yii::$app->db->createCommand()->batchInsert('!ffoms_worked', [
            'nomer_z',
            'snils',
            'dact',
            'pi',
            'id_zl',
            'fam',
            'im',
            'ot',
            'dostdr',
            'dr',
            'w',
            'address_r',
            'index',
            'address_reg',
            'name_doc',
            's_doc',
            'n_doc',
            'data_doc',
        ], $this->zl)->execute();
        $this->zl = [];
        $this->zl_inserted += $i;
        if ($info)
            echo $this->zl_inserted . ' from ' . $this->zl_count . PHP_EOL;
    }

    /**
     * @param $reader self
     * @return bool
     */
    protected function callbackZL($reader)
    {
        $xml = $reader->expandSimpleXml();
		$n = $this->getSafeValue($xml, 'nomer_z');
		if($n <= $this->n_start)
			return true;
        $a = [];
        $a['nomer_z'] = $n;
        $a['snils'] = $this->getSafeValue($xml, 'snils');
        $a['dact'] = $this->getSafeValue($xml, 'dact');
        $a['pi'] = $this->getSafeValue($xml, 'pi');
        $a['id_zl'] = $this->getSafeValue($xml, 'id_zl');
        $a['fam'] = $this->getSafeValue($xml, 'fam');
        $a['im'] = $this->getSafeValue($xml, 'im');
        $a['ot'] = $this->getSafeValue($xml, 'ot');
        $a['dostdr'] = $this->getSafeValue($xml, 'dostdr');
        $a['dr'] = $this->getSafeValue($xml, 'dr');
        $a['w'] = $this->getSafeValue($xml, 'w');
        $a['address_r'] = $this->getSafeValue($xml, 'address_r');
        $a['index'] = $this->getSafeValue($xml, 'index');
        $a['address_reg'] = $this->getSafeValue($xml, 'address_reg');
        $a['name_doc'] = $this->getSafeValue($xml->doc, 'name_doc');
        $a['s_doc'] = $this->getSafeValue($xml->doc, 's_doc');
        $a['n_doc'] = $this->getSafeValue($xml->doc, 'n_doc');
        $a['data_doc'] = $this->getSafeValue($xml->doc, 'data_doc');
        $this->zl[] = $a;

        if (count($this->zl) >= 1000) {
            $this->insertZl();
        }
        return true;
    }

    protected function callbackZGLV($reader)
    {
        $xml = $reader->expandSimpleXml();
        $this->zl_count = (int)$this->getSafeValue($xml, 'nrec');
        return true;
    }

    protected function getSafeValue($xml, $attr)
    {
        //$attr = strtoupper($attr);
        if (!isset($xml->$attr))
            return null;
        return (string)($xml->$attr);
    }
}