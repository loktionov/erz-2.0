<?php
return [
    'adminEmail' => 'skfoms@gmail.com',
    'region' => '26',
    'okato' => '07000',
    'dateFormat'=>'d.m.Y',
    'dateTimeFormat'=>'d.m.Y H:i:s',
    'myDateFormat'=>'Y.m.d',
    'myDateTimeFormat'=>'Y.m.d H:i:s',
];
