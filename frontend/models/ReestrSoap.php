<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.12.2015
 * Time: 12:25
 */

namespace frontend\models;

/** @soap-indicator all */
class ReestrSoap extends Reestr
{
    /**
     * @var integer
     * @soap
     */
    public $id;
    /**
     * @var string
     * @soap
     */
    public $ENumber;

    /**
     * @var integer
     * @soap
     */
    public $ManId;
    /**
     * @var string
     * @soap
     */
    public $orgId;
    /**
     * @var integer
     * @soap
     */
    public $typePoles;
    /**
     * @var string
     * @soap
     */
    public $beginDate;
    /**
     * @var string
     * @soap
     */
    public $endDate;
    /**
     * @var string
     * @soap
     */
    public $stmDate;
    /**
     * @var integer
     * @soap
     */
    public $stmType;
    /**
     * @var integer
     * @soap
     */
    public $formPoles;
    /**
     * @var integer
     * @soap
     */
    public $reasonStm;
    /**
     * @var string
     * @soap
     */
    public $numBlank;
    /**
     * @var string
     * @soap
     */
    public $numCard;
    /**
     * @var integer
     * @soap
     */
    public $statusPoles;
    /**
     * @var string
     * @soap
     */
    public $AnnulDate;
    /**
     * @var string
     * @soap
     */
    public $InsDate;
    /**
     * @var integer
     * @soap
     */
    public $PVCode;

}