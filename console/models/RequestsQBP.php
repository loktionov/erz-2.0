<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 07.07.2016
 * Time: 14:59
 */

namespace console\models;


class RequestsQBP extends RequestsConsole
{
    const BODY_TEG = 'QBP';
    const SCENARIO_ZP1 = 'ZP1';
    const SCENARIO_ZP2 = 'ZP2';
    const SCENARIO_ZP4 = 'ZP4';
    const SCENARIO_ZP6   = 'ZP6';
    const SCENARIO_ZP9   = 'ZP9';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $default = $scenarios[self::SCENARIO_ZP1];
        $scenarios[self::SCENARIO_ZP1] = array_merge($default, [
            'typedoc', 'serdoc', 'numdoc', 'snils', 'enp',
        ]);
        $scenarios[self::SCENARIO_ZP2] = array_merge($default, [

        ]);
        $scenarios[self::SCENARIO_ZP4] = array_merge($default, [

        ]);
        $scenarios[self::SCENARIO_ZP6] = array_merge($default, [

        ]);
        $scenarios[self::SCENARIO_ZP9] = array_merge($default, [

        ]);
        return $scenarios;
    }
    
    public function __construct(array $config = ['scenario' => self::SCENARIO_ZP1])
    {
        parent::__construct($config);
    }

    public function getXmlBody($body_teg = self::BODY_TEG){
        $body_teg = $body_teg . '_' . $this->scenario;
        $x = new \DOMDocument('1.0', 'Windows-1251');
        $body = $x->appendChild($x->createElement($body_teg));
        $msh = $body->appendChild($x->createElement('MSH'));
        $msh->appendChild($x->createElement('MSH.1', '|'));
        $msh->appendChild($x->createElement('MSH.2', '^~\&amp;'));
        $msh3 = $msh->appendChild($x->createElement('MSH.3'));
        $msh3->appendChild($x->createElement('HD.1', iconv('utf-8', 'cp1251','СРЗ ') . \Yii::$app->params['region']));
        $s = $x->saveXML();
        

        /*$x = new \SimpleXMLElement("<$body_teg></$body_teg>", LIBXML_NOXMLDECL);
        $msh = $x->addChild('MSH');
        $msh->addChild('MSH.1', '|');
        $msh->addChild('MSH.2', '^~\&amp;');
        $msh3 = $msh->addChild('MSH.3');
        $msh3->addChild('HD.1', 'СРЗ ' . \Yii::$app->params['region']);
        $s = $x->asXML();*/
    }

}