<?php

use kartik\mpdf\Pdf;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.tfomssk.ru',
                'username' => 'support@tfomssk.ru',
                'password' => 'JFth7#',
                'port' => '25',
                //'encryption' => 'tls',
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlsrv:Server=srv-registr\sql;Database=rmp',
            'username' => '',
            'password' => '',
            //'charset' => 'utf8',
        ],
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => 'A4',
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            // refer settings section for all configuration options
        ],
    ],
];
