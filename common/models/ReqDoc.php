<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "req_doc".
 *
 * @property integer $id
 * @property integer $rid
 * @property integer $typedoc
 * @property string $serdoc
 * @property string $numdoc
 * @property string $datedoc
 * @property string $doc_date_end
 * @property integer $prev
 */
class ReqDoc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'req_doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rid'], 'required'],
            [['rid', 'typedoc', 'prev'], 'integer'],
            [['serdoc', 'numdoc'], 'string'],
            [['datedoc', 'doc_date_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rid' => 'RequestsID',
            'typedoc' => 'Тип документа',
            'serdoc' => 'Серия Документа',
            'numdoc' => 'Номер документа',
            'datedoc' => 'Дата начала документа',
            'doc_date_end' => 'Дата окончания документа',
            'prev' => 'Предыдущий',
        ];
    }
}
