<?php

namespace common\models;

use common\components\ConsoleActiveRecord;
use Yii;

/**
 * This is the model class for table "disp_ident".
 *
 * @property integer $id
 * @property integer $cid
 * @property integer $pid
 * @property integer $smo
 */
class DispIdent extends ConsoleActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_ident';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'pid', 'smo'], 'required'],
            [['cid', 'pid', 'smo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cid' => 'Cid',
            'pid' => 'Pid',
            'smo' => 'Smo',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LINK] = ['pid', 'smo'];
        return $scenarios;
    }

}
