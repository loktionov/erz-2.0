<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'region' => '26',
    'okato' => '07000',
    'disp_date' => '01.01.2017',
];
