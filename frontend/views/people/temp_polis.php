<?php
/**
 * @var $data \frontend\models\ApplyView
 * @var $reg \common\models\PeopleAddress
 * @var $live \common\models\PeopleAddress
 */
?>

<!doctype html>
<html>

<body>
<div class="a5">

    <table id="tmp-smo">
        <tr>
            <td><?= $data->smo; ?></td>
        </tr>
        <tr>
            <td><?= $data->smo_addr; ?></td>
        </tr>
    </table>
    <?php $stmdate = \frontend\models\ApplyView::SplitDate($data->stmdate) ?>
    <table id="tmp-stmdate">
        <tbody>
        <tr>
            <td class="tmp-day"><?= $stmdate[0] ?></td>
            <td class="tmp-month"><?= $stmdate[1] ?></td>
            <td class="tmp-year"><?= $stmdate[2] ?></td>
        </tr>
        </tbody>
    </table>

    <table id="tmp-fio">
    <tbody>
        <tr>
            <td><?= $data->getFIO(); ?></td>
        </tr>
    </tbody>
    </table>

    <table id="tmp-pasp">
        <tbody>
        <tr>
            <td><?= implode('&nbsp;', [$data->dateMan, $data->doc, $data->serDoc, $data->numDoc, 'от', $data->dateDoc]); ?></td>
        </tr>
        <tr>
            <td><?= $data->orgdoc; ?></td>
        </tr>
        </tbody>
    </table>
    <table id="tmp-pbman">
        <tbody>
        <tr>
            <td><?= $data->pbMan; ?></td>
        </tr>
        </tbody>
    </table>
    <table id="tmp-sexman">
        <tbody>
        <tr>
            <td id="tmp-sexm" class="checkbox <?= $data->sexMan == 1 ? 'check' : '' ?>"> </td>
            <td id="tmp-sexprobel"> </td>
            <td id="tmp-sexw" class="checkbox <?= $data->sexMan == 2 ? 'check' : '' ?>"> </td>
        </tr>
        </tbody>
    </table>
    <?php $enddate = \frontend\models\ApplyView::SplitDate($data->enddate) ?>
    <table id="tmp-enddate">
        <tbody>
        <tr>
            <td class="tmp-day"><?= $enddate[0] ?></td>
            <td class="tmp-month1"><?= $enddate[1] ?></td>
            <td class="tmp-year"><?= $enddate[2] ?></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>