<?php
/* @var $this yii\web\View */
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'ЕРЗ 2.0';
$dataProvider = new ActiveDataProvider([
    'query' => \frontend\models\Typedocument::find(),
    'pagination' => [
        'pageSize' => 5,
    ],
]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
]);

