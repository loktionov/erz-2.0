<?php
use yii\widgets\DetailView;

/**
 * @var $model frontend\models\People
 */
?>
<?=
\yii\grid\GridView::widget(
    [
        'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getReestrs()]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'ENumber',
        ],
    ]
); ?>

<?=
DetailView::widget(
    [
        'model' => $model->getStickMo(),
        'attributes' => [
            'MOCode',
            'DocCode',
        ],
    ]
); ?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'ENP',
        'Name',
        'sName',
        'pName',
        'dateMan',
        'sexMan',
        'pbMan',
        'nationMan',
        'typeDoc',
        'serDoc',
        'numDoc',
        'dateDoc',
        'statusMan',
        'regKladrId',
        'addressReg',
        'dateReg',
        'addressLive',
        'activPolesId',
        'dateDeath',
        'liveKladrId',
        'snils',
        'okato_mj',
        'InsDate',
        'cr',
        'crdate',
        'lostman',
        'proxy_man',
        'contact',
        'doc_date_end',
        'livetemp',
        'refugee',
        'orgdoc',
        'FFOMS',
        'wid',
        'okato_reg',
        'kladr_reg',
        'kladr_live',
    ],
]) ?>