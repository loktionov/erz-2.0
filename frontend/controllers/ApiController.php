<?

namespace frontend\controllers;

use frontend\components\SoapAction;
use frontend\models\People;
use frontend\models\PeopleSoap;
use frontend\models\PeopleSoapOut;
use frontend\models\Reestr;
use frontend\models\ReestrSoap;
use frontend\models\StickMo;
use frontend\models\StickMoSoap;

class ApiController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'soap' => SoapAction::className(),
        ];
    }

    /**
     * @param frontend\models\PeopleSoap $people_in
     * @return frontend\models\PeopleSoapOut
     * @soap
     */
    public function getPeople($people_in)
    {
        $people_out = new PeopleSoapOut(array());
        $people_in = new PeopleSoap((array)$people_in);

        if (!$people_in->validate()) {
            $people_out->result = array_merge(PeopleSoapOut::getResultArray(PeopleSoapOut::RESULT_ERROR), $people_in->errors);
            return $people_out;
        }

        $attrs = $people_in->attributes;
        if (empty($people_in->snils))
            unset($attrs['snils']);
        if (empty($people_in->typeDoc)) {
            unset($attrs['typeDoc']);
            unset($attrs['serDoc']);
            unset($attrs['numDoc']);
        }
        $attrs['lostman'] = 1;
        $attrs['wid'] = null;
        $people = People::find()->where($attrs)->all();

        if (count($people) == 0) {
            $people_out->result = PeopleSoapOut::getResultArray(PeopleSoapOut::RESULT_NULL);
            return $people_out;
        }
        if (count($people) > 1) {
            $people_out->result = PeopleSoapOut::getResultArray(PeopleSoapOut::RESULT_DOUBLE);
            return $people_out;
        }
        $wid = People::find()->where(['ENP' => $people[0]->ENP, 'wid' => null, 'lostman' => 1])->all();
        if (count($wid) > 1) {
            $people_out->result = PeopleSoapOut::getResultArray(PeopleSoapOut::RESULT_ERROR);
            return $people_out;
        }

        $people_out->result = PeopleSoapOut::getResultArray(PeopleSoapOut::RESULT_OK);
        $people_out->attributes = $people[0]->attributes;
        /** @var Reestr $reestr */
        $reestr = $people[0]->activeReestr;
        $reestr->orgId = $reestr->org->NAME;
        $people_out->reestr = new ReestrSoap();
        $people_out->reestr->attributes = $reestr->attributes;

        $people_out->stickmo = new StickMoSoap();

        $stick = $people[0]->StickMo;
        if (isset($stick->MOCode)) {
            $mo = (new \yii\db\Query())
                ->select(['NAMMO'])
                ->from('MO')
                ->where(['KODMO' => $stick->MOCode])
                ->one();
            //$people_out->stickmo->MOCode = htmlspecialchars($mo['NAMMO']);
            $people_out->stickmo->MOCode = $mo['NAMMO'];
            $people_out->stickmo->Reason = $stick->Reason;
        }

        return $people_out;
    }
}