<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "disp_mapoms_view".
 *
 * @property string $kod
 * @property string $mo
 * @property integer $1
 * @property integer $2
 * @property integer $3
 * @property integer $4
 * @property integer $5
 * @property integer $6
 * @property integer $7
 * @property integer $8
 * @property integer $9
 * @property integer $10
 * @property integer $11
 * @property integer $12
 * @property integer $mapoms
 */
class DispMapomsView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disp_mapoms_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod', 'mo'], 'string'],
            [['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'mapoms'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod' => 'Kod',
            'mo' => 'Mo',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10',
            '11' => '11',
            '12' => '12',
            'mapoms' => 'Mapoms',
        ];
    }
}
