<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 03.06.2016
 * Time: 17:07
 */

namespace common\components;


use yii\base\Model;
use yii\validators\StringValidator;

class SnilsValidator extends StringValidator
{

    private function getRegexp()
    {
        return [
            '/^\d{3}-\d{3}-\d{3}\s\d\d$/iu' => true,
        ];
    }

    /**
     * @@inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        parent::validateAttribute($model, $attribute);
        if ($model->hasErrors($attribute))
            return;

        foreach ($this->getRegexp() as $q => $match) {
            if (!preg_match($q, $model->$attribute) == $match) {
                $this->addFormatError($model, $attribute);
                return;
            }
        }
        if(!self::checkSNILS($model->$attribute))
            $model->addError($attribute, 'Неверная контрольная цифра СНИЛС');
        return;
    }

    public static function checkSNILS($snils)
    {
        $regex = '/(\d)/';
        preg_match_all($regex, $snils, $matches);
        $matches = $matches[0];
        $end_check = (int)($matches[9] . $matches[10]);
        $minsnils = 1001998;
        $oursnils = '';
        $checksum = 0;
        for ($i = 0; $i < 9; $i++) {
            $num = (int)$matches[$i];
            $oursnils .= $matches[$i];
            $checksum += $num * (9 - $i);
        }
        //echo $oursnils.' - '.$checksum;
        if ($minsnils >= (int)$oursnils)
            return true;
        if ($checksum < 100) {
            if ($checksum != $end_check)
                return false;
            else return true;
        } elseif ($checksum == 100 OR $checksum == 101) {
            if ($end_check != 0)
                return false;
            else return true;
        } elseif ($checksum > 101) {
            $checksum = $checksum % 101;
            if ($end_check != (int)(substr((string)$checksum, -2)))
                return false;
            else return true;
        } else return false;
    }

    /**
     * @param $model Model
     * @param $attribute string
     */
    private function addFormatError($model, $attribute)
    {
        $model->addError($attribute, "Неверный формат поля «{$model->getAttributeLabel($attribute)}».");
    }

}