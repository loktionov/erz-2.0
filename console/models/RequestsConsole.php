<?php

namespace console\models;

use common\components\NameValidator;
use common\models\ReqDoc;
use common\models\ReqPers;
use common\models\ReqPolis;
use common\models\RequestsBase;
use yii;

class RequestsConsole extends RequestsBase
{    
    const SCENARIO_ACK = 'ACK';
    const SCENARIO_ADT_A01 = 'ADT_A01';
    const SCENARIO_ADT_A03 = 'ADT_A03';//снятие
    const SCENARIO_ADT_A24 = 'ADT_A24';//объединение
    const SCENARIO_ADT_A37 = 'ADT_A37';//разделение
    const SCENARIO_ZPI_ZWI = 'ZPI_ZWI';//п20
    
    const SCENARIO_ZPI_ZA7 = 'ZPI_ZA7';//goznak    

    public function getDoc(){
        return $this->hasMany(ReqDoc::className(), ['rid' => 'rid']);
    }
    public function getPers(){
        return $this->hasMany(ReqPers::className(), ['rid' => 'rid']);
    }
    public function getPolis(){
        return $this->hasMany(ReqPolis::className(), ['rid' => 'rid']);
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['sname', 'name', 'pname', 'prev_sname', 'prev_name', 'prev_pname',], NameValidator::className()];
        return $rules;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] = ['rid', 'pid', 'p', 'stmdate'];
        return $scenarios;
    }

    public static function createQBP_ZP1($pid, $p = self::SCENARIO_QBP_ZP1)
    {
        $q = 'SELECT pid, enp,pbman,prev_enp	,prev_sname,prev_name,prev_pname,dateman,sexman,typedoc,serdoc,numdoc,
prev_typedoc,prev_serdoc,prev_numdoc,sname,[name],pname,snils,qpd10 AS typepoles,qpd11 AS enumber,:p AS p,getdate() insdate,
convert(DATE, getdate()) stmdate FROM [dbo].[fnQBP_zp1] (:pid)';
        $people = Yii::$app->db->createCommand($q, [':p' => $p, ':pid' => $pid])->queryOne();
        if (empty($people))
            return false;

        $req = new self(['scenario' => $p]);
        $req->attributes = $people;        
        $v = $req->validate();
        return 'ok';
    }
}
