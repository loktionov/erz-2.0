<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StickMO */

$this->title = 'Обновить ' . $fio;
$this->params['breadcrumbs'][] = ['label' => 'Stick Mos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stick-mo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
