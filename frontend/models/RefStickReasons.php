<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ref_StickReasons".
 *
 * @property integer $id
 * @property string $ReasonName
 */
class RefStickReasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_StickReasons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['ReasonName'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ReasonName' => 'Reason Name',
        ];
    }
}
