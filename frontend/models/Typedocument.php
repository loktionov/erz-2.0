<?php

namespace frontend\models;

use common\models\Documents;
use Yii;

/**
 * This is the model class for table "typedocument_new".
 *
 * @property integer $id
 * @property string $Name
 * @property string $Comment
 * @property string $docser
 * @property string $docnum
 * @property integer $isAllow
 * @property string $pfr
 * @property integer $zags
 *
 * @property Documents[] $documents
 */
class Typedocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typedocument_new';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'isAllow', 'zags'], 'integer'],
            [['Name', 'Comment', 'docser', 'docnum', 'pfr'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'Comment' => 'Comment',
            'docser' => 'Docser',
            'docnum' => 'Docnum',
            'isAllow' => 'Is Allow',
            'pfr' => 'Pfr',
            'zags' => 'Zags',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['typedoc' => 'id']);
    }
}
