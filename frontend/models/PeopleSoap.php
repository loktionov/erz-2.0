<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 25.12.2015
 * Time: 12:01
 */

namespace frontend\models;

use yii\base\Model;

/** @soap-indicator all */
class PeopleSoap extends Model
{
    public function rules()
    {
        return [
            [
                ['Name', 'sName', 'sexMan', 'dateMan'], 'required'
            ],
            [
                ['sexMan', 'typeDoc'], 'integer'
            ],
            [
                ['dateMan'], 'date', 'format' => 'php:d.m.Y'
            ],
            [['typeDoc', 'snils'], 'default', 'value' => 0],
            [
                ['typeDoc'], 'OrAttributes', 'params' => ['OR' => 'snils']
            ],

        ];
    }

    /**
     * @param string $attribute имя поля, которое будем валидировать
     * @param array $params дополнительные параметры для правила валидации
     */
    public function OrAttributes($attribute, $params)
    {
        if (empty($this->$attribute) AND empty($this->$params['OR'])) {
            $this->addError($attribute, 'Должны быть заполнены либо '
                . $this->getAttributeLabel($attribute) . ' либо ' . $this->getAttributeLabel($params['OR']));
        }
    }

    /**
     * @var string
     * @soap
     */
    public $Name;

    /**
     * @var string
     * @soap
     */
    public $sName;

    /**
     * @var string
     * @soap
     */
    public $pName;

    /**
     * @var string
     * @soap
     */
    public $dateMan;

    /**
     * @var string
     * @soap
     */
    public $sexMan;

    /**
     * @var string
     * @soap
     */
    public $typeDoc;

    /**
     * @var string
     * @soap
     */
    public $serDoc;

    /**
     * @var string
     * @soap
     */
    public $numDoc;

    /**
     * @var string
     * @soap
     */
    public $snils;

}